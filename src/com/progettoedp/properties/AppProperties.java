package com.progettoedp.properties;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class AppProperties {
	
	private static AppProperties instance = null;
	
	 public static AppProperties getInstance() {
	      if(instance == null) {
	         instance = new AppProperties();
	      }
	      return instance;
	   }

	public void create(){
	 
		Properties prop = new Properties();
		OutputStream output = null;

		try {

			output = new FileOutputStream("application.properties");
			 
			prop.setProperty("partita_iva", "00178340261");
			prop.setProperty("application_type", "FATEL");
			prop.setProperty("host_name", "192.168.50.6");
			prop.setProperty("port_number", "1433");
			prop.setProperty("database_name", "FATEL");
			prop.setProperty("database_eq", "dbo_STARMOVFATT4_m");
			prop.setProperty("table_name", "SOL1GFAT_MAST");
			prop.setProperty("table_name_eq", "STARMOVFATT4_m");
			prop.setProperty("user_name", "sa");
			prop.setProperty("password", "milkuht");
			prop.setProperty("xml_path", "\\\\192.168.50.6\\fatel\\documenti\\xml\\sol1g\\");
			prop.setProperty("pdf_path", "\\\\192.168.50.6\\pdf\\");
			prop.setProperty("attach_path", "\\\\192.168.50.6\\attach\\");
			prop.setProperty("sub_days", "7");
			prop.setProperty("active_invoice", "false");
			prop.setProperty("database_type", "SQLITE");
			prop.setProperty("library_as400", "IT_SOL_F");
			prop.setProperty("library_as400_cont", "BMS60L_DAT");
			prop.setProperty("tipo_protocollo", "ITACA");
			prop.setProperty("filtro_tipo_documento", "TD16|TD17|TD18");
			
			// save properties to project root folder
			prop.store(output, null);

		} catch (IOException io) {
			io.printStackTrace();
		} finally {
			if (output != null) {
				try {
					output.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		}
	}
	
	public Map<String, String> load(){
		
		Map<String, String> map = new HashMap<String, String>();
		
		Properties prop = new Properties();
		InputStream input = null;

		try {

			input = new FileInputStream("application.properties");

			// load a properties file
			prop.load(input);
			
			for (final String name: prop.stringPropertyNames()) {
				map.put(name, prop.getProperty(name));
			}
			 

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		return map;
	}
	
 
	
}
