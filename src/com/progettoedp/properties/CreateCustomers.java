package com.progettoedp.properties;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import com.progettoedp.dao.DAOAS400;
import com.progettoedp.data.Customer;
import com.progettoedp.util.Logger;

public class CreateCustomers {

	private static final Logger logger = Logger.getLogger(DAOAS400.class);
	private static final String TAG = "CreateCustomers";
	
	private static final String customersFile="customers.csv";
	private PrintWriter writer;
	private BufferedReader reader;


	public void create(){
		try {
			File f = new File(customersFile);
			if(!f.exists()){

				writer = new PrintWriter(customersFile);

				StringBuilder sb = new StringBuilder();
				sb.append("partita_iva");
				sb.append(";");
				sb.append("separatore");
				sb.append(";");
				sb.append("occorrenza");
				sb.append(";");
				sb.append("allineamento");
				sb.append("dx");
				sb.append(";");

				writer.write(sb.toString());
				writer.close();

			}


		} catch (Exception e) {
			logger.error(TAG, e);

		}

	}

	public List<Customer> loadCustomers(){

		List<Customer> customers = new ArrayList<Customer>();
		String line = "";

		try {
			reader = new BufferedReader(new FileReader(customersFile));

			while ((line = reader.readLine()) != null)  {
				String[] data = line.split(";");
				Customer customer = new Customer();
				customer.setPartitaIva(data[0]);
				customer.setSeparatore(data[1]);
				customer.setOccorrenza(Integer.parseInt(data[2]));
				customer.setAllineamento(data[3]);
				customers.add(customer);
			}


		} catch (Exception e) {
			logger.error(TAG, e);
		}

		return customers;

	}



}
