package com.progettoedp.main;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.jdesktop.application.SingleFrameApplication;

import com.progettoedp.dao.DAO;
import com.progettoedp.dao.DatabaseManager;
import com.progettoedp.data.Customer;
import com.progettoedp.logic.Import;
import com.progettoedp.logic.Migrate;
import com.progettoedp.logic.NotImported;
import com.progettoedp.logic.Reimport;
import com.progettoedp.properties.AppProperties;
import com.progettoedp.properties.CreateCustomers;
import com.progettoedp.testo.Testo;


public class Application extends SingleFrameApplication{

	private DatabaseManager sqliteManager;
	private DatabaseManager mssqlManager;
	private DatabaseManager mssqlManagerApp;
	private DatabaseManager mssqlManagerEq;
	private DatabaseManager as400Manager;
	private DAO dao;
	private String applicationType;
	private String hostName;
	private String portNumber;
	private String databaseName;
	private String user;
	private String password;
	private String partitaIva;
	private String xmlPath;
	private String pdfPath;
	private String attachPath;
	private String xmlAs400Path;
	private String tableName;
	private String tableNameEq;
	private String printerName;
	private String host_as400;
	private String user_as400;
	private String password_as400;
	private String library_as400;
	private String library_as400_cont;
	private String databaseNameEq;
	private int subbedDays;
	private boolean activeInvoice;
	private String[] filteredDocuments;
	private String operation;
	private String databaseType;
	private String tipoProtocollo;
	private String xmlOrderPath;
	private List<Customer> customers;

	public void init()  {

		CreateCustomers createCustomers = new CreateCustomers();
		createCustomers.create();
		customers = createCustomers.loadCustomers();
		
		
		readProperties();
		if (operation != null){
//			applicationType = Testo.REIMPORT;
			applicationType = operation.trim();
		}
		 
		sqliteManager = new DatabaseManager(Testo.SQLITE, null);
		mssqlManagerApp = new DatabaseManager(Testo.MSSQL, Testo.FATTUREB2B);
		mssqlManager =  new DatabaseManager(Testo.MSSQL, databaseName);
		mssqlManagerEq =  new DatabaseManager(Testo.MSSQL, databaseNameEq);
		as400Manager =  new DatabaseManager(Testo.AS400, null);
		dao = DAO.create(sqliteManager, mssqlManager, as400Manager, mssqlManagerEq, mssqlManagerApp);
		sqliteManager.init();
		mssqlManagerApp.init();
		mssqlManager.init();
		mssqlManagerEq.init();
		as400Manager.init();
		dao.setType(databaseType);
		dao.init(databaseType);  

		try {
			switch (applicationType) {
			case Testo.FATEL:
				Import importa = new Import(dao);
 				importa.run();
 				importa.runOrder();

				break;
			case Testo.TEST:
				Import testImport = new Import(dao);
				testImport.runTest();
				break;
			case Testo.LOCAL:
				Import newImport = new Import(dao);
				newImport.runLocal();
				break;
			case Testo.IX:
				break;
			case Testo.REIMPORT:
				Reimport reimport = new Reimport(dao, Testo.REIMPORT);
				reimport.run();
				break;
			case Testo.MIGRATE:
				Migrate migrate = new Migrate(dao);
				migrate.run();
				break;
			case Testo.NOT_IMPORTED:
				NotImported notImported = new NotImported(dao, Testo.NOT_IMPORTED);
				notImported.run();
				break;
			default:
				break;
			}

		} catch (Exception e) {

		}  


	}

	public void readProperties(){

		File f = new File("application.properties");
		if(!f.exists()) { 
			AppProperties.getInstance().create();
		}

		Map<String, String> map = AppProperties.getInstance().load();
		partitaIva = map.get("partita_iva");
		applicationType = map.get("application_type");
		hostName = map.get("host_name");
		portNumber = map.get("port_number");
		databaseName = map.get("database_name");
		user = map.get("user_name");
		password = map.get("password");
		xmlPath = map.get("xml_path");
		pdfPath = map.get("pdf_path");
		tableName = map.get("table_name");
		tableNameEq = map.get("table_name_eq");
		attachPath = map.get("attach_path");
		xmlAs400Path = map.get("xml_as400_path");
		printerName = map.get("printer_name");
		host_as400 = map.get("host_as400");
		user_as400 = map.get("user_as400");
		password_as400 = map.get("password_as400");
		library_as400 = map.get("library_as400");
		library_as400_cont = map.get("library_as400_cont");
		databaseNameEq = map.get("database_eq");
		databaseType = map.get("database_type");
		subbedDays = Integer.parseInt(map.get("sub_days"));
		activeInvoice = Boolean.parseBoolean(map.get("active_invoice"));
		tipoProtocollo = map.get("tipo_protocollo");
		xmlOrderPath = map.get("xml_order_path");
		filteredDocuments = map.get("filtro_tipo_documento").toString().split("\\|");
	}



	@Override
	protected void initialize(String[] args) {
		if (args.length >0 ){
			operation = args[0];	
		}
		super.initialize(args);
	}



	public static synchronized Application getInstance() {
		return (Application) org.jdesktop.application.Application.getInstance();
	}

	public DatabaseManager getSqliteManager() {
		return sqliteManager;
	}

	public void setSqliteManager(DatabaseManager sqliteManager) {
		this.sqliteManager = sqliteManager;
	}

	public DAO getDao() {
		return dao;
	}

	public void setDao(DAO dao) {
		this.dao = dao;
	}

	@Override
	protected void shutdown() {
		System.exit(1);
	}

	@Override
	protected void startup() {
		init();
	}

	public String getApplicationType() {
		return applicationType;
	}


	public String getHostName() {
		return hostName;
	}


	public String getPortNumber() {
		return portNumber;
	}


	public String getDatabaseName() {
		return databaseName;
	}

	public String getUser() {
		return user;
	}


	public String getPassword() {
		return password;
	}

	public String getPartitaIva() {
		return partitaIva;
	}

	public String getXmlPath() {
		return xmlPath;
	}

	public String getPdfPath() {
		return pdfPath;
	}

	public String getTableName() {
		return tableName;
	}

	public String getAttachPath() {
		return attachPath;
	}

	public void setAttachPath(String attachPath) {
		this.attachPath = attachPath;
	}

	public String getPrinterName() {
		return printerName;
	}

	public void setPrinterName(String printerName) {
		this.printerName = printerName;
	}

	public String getHost_as400() {
		return host_as400;
	}

	public void setHost_as400(String host_as400) {
		this.host_as400 = host_as400;
	}

	public String getUser_as400() {
		return user_as400;
	}

	public void setUser_as400(String user_as400) {
		this.user_as400 = user_as400;
	}

	public String getPassword_as400() {
		return password_as400;
	}

	public void setPassword_as400(String password_as400) {
		this.password_as400 = password_as400;
	}

	public String getLibrary_as400() {
		return library_as400;
	}

	public void setLibrary_as400(String library_as400) {
		this.library_as400 = library_as400;
	}
	
	public String getLibrary_as400_cont() {
		return library_as400_cont;
	}

	public void setLibrary_as400_cont(String library_as400_cont) {
		this.library_as400_cont = library_as400_cont;
	}

	public String getDatabaseNameEq() {
		return databaseNameEq;
	}

	public void setDatabaseNameEq(String databaseNameEq) {
		this.databaseNameEq = databaseNameEq;
	}

	public int getSubbedDays() {
		return subbedDays;
	}

	public void setSubbedDays(int subbedDays) {
		this.subbedDays = subbedDays;
	}

	public String getTableNameEq() {
		return tableNameEq;
	}

	public void setTableNameEq(String tableNameEq) {
		this.tableNameEq = tableNameEq;
	}

	public boolean isActiveInvoice() {
		return activeInvoice;
	}

	public void setActiveInvoice(boolean activeInvoice) {
		this.activeInvoice = activeInvoice;
	}

	public String getXmlAs400Path() {
		return xmlAs400Path;
	}

	public void setXmlAs400Path(String xmlAs400Path) {
		this.xmlAs400Path = xmlAs400Path;
	}

	public String getTipoProtocollo() {
		return tipoProtocollo;
	}

	public void setTipoProtocollo(String tipoProtocollo) {
		this.tipoProtocollo = tipoProtocollo;
	}

	public String getXmlOrderPath() {
		return xmlOrderPath;
	}

	public void setXmlOrderPath(String xmlOrderPath) {
		this.xmlOrderPath = xmlOrderPath;
	}

	public List<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(List<Customer> customers) {
		this.customers = customers;
	}

	public String[] getFilteredDocuments() {
		return filteredDocuments;
	}

	public void setFilteredDocuments(String[] filteredDocuments) {
		this.filteredDocuments = filteredDocuments;
	}
	
}
