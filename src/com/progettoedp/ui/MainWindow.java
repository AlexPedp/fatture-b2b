package com.progettoedp.ui;
 

import java.awt.event.ActionEvent;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;

import com.progettoedp.main.Application;
import com.progettoedp.properties.AppProperties;

public class MainWindow extends JFrame {

	private String azienda;
	private AppProperties appProperties;
	private File file;
	private JButton conferma;
	private JButton annulla;
	private JLabel label;
	private JComboBox<String> combo;
	private String[] types = new String[] {"Normal", "Migrate",
            "Re-Import", "Local"};
	
	public MainWindow() {
		 	initComponents();
		    add(label);
		    add(combo);
		    add(conferma);
		    add(annulla);
	}
	
	public void initComponents(){
		conferma =new JButton("Confirm");    
		conferma.setBounds(50,100,100, 25); 
		conferma.setText("Conferma");
		conferma.addActionListener(new java.awt.event.ActionListener() {
	            public void actionPerformed(java.awt.event.ActionEvent evt) {
	                ButtonConferma(evt);
	            }
	        });
		
		annulla =new JButton("Cancel");    
		annulla.setBounds(160,100,100, 25); 
		annulla.addActionListener(new java.awt.event.ActionListener() {
	            public void actionPerformed(java.awt.event.ActionEvent evt) {
	                ButtonAnnulla(evt);
	            }
	        });

				 
		label = new JLabel();		
		label.setText("Type:");
		label.setBounds(70, 30, 100, 25);
		
		combo = new JComboBox<>(types);
		combo.setSelectedIndex(3);
		combo.setBounds(110, 30, 100, 25);
		
		setSize(300, 180);  
		setLocationRelativeTo(null);
	    setResizable(false);
	    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
		setLayout(null);  
	}

	public void display(){
		 setTitle("Fatture-b2b GUI");
         setVisible(true);
   }
 
	private void ButtonConferma(ActionEvent evt) {
		String value = combo.getSelectedItem().toString().toUpperCase();
		String[] args = new String[] {value};
		Application.launch(Application.class, args);
	}

	private void ButtonAnnulla(ActionEvent evt) {
		dispose();
	}
	
	 

}

