package com.progettoedp.receipt;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.AS400Message;
import com.ibm.as400.access.CommandCall;
import com.ibm.as400.access.ProgramCall;
import com.progettoedp.util.Logger;

public class RemoteCommand {

	private static final Logger logger = Logger.getLogger(RemoteCommand.class);
	private static final String TAG = "RemoteCommand";
	private static final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

	//	private static final String HOST = "192.168.50.1"; 
	//	private static final String UID = "ITACA";  
	//	private static final String PWD = "ACATI";  
	private String host;
	private String uid;
	private String pwd;

	public RemoteCommand(String host, String uid, String pwd){
		this.host = host;
		this.uid = uid;
		this.pwd = pwd;
	}


	public void run() {

		String fullProgramName = "/QSYS.LIB/ACK60L_OBJ.LIB/FA014CL.PGM";

		AS400 as400 = null;
	 	ProgramCall programCall;
		try {
			
			logger.debug(TAG, "Remote command AS400 started at: " + sdf.format(new Date()));
			as400 = new AS400(host, uid, pwd);
		 
			programCall = new ProgramCall(as400);
			programCall.setProgram(fullProgramName);

			CommandCall command = new CommandCall(as400);

			command.run("ADDLIBLE LIB(MMAIL)");
			command.run("ADDLIBLE LIB(KEYTOOL)");
			command.run("ADDLIBLE LIB(PLEX510)");
			command.run("ADDLIBLE LIB(PCNFE_SOL)");
			command.run("ADDLIBLE LIB(DA_V02_O)");
			command.run("ADDLIBLE LIB(PCNTJRN)");
			command.run("ADDLIBLE LIB(ACK60L_OBJ)");
			command.run("ADDLIBLE LIB(ACK60L_OBP)");
			command.run("ADDLIBLE LIB(BMS60L_DAT)");
			command.run("ADDLIBLE LIB(KMS60L_DAT)");
			command.run("ADDLIBLE LIB(IT_V02_O)");
			command.run("ADDLIBLE LIB(IT_SOL_F)");
			command.run("ADDLIBLE LIB(IT_SOL_PO)");
			command.run("ADDLIBLE LIB(IT_SOL_PF)");
			command.run("ADDLIBLE LIB(QTEMP)");
			command.run("ADDLIBLE LIB(QGPL)");
			command.run("ADDLIBLE LIB(GA_V01_O)");


			if (!programCall.run()) {
				AS400Message[] messageList = programCall.getMessageList();
				for (AS400Message message : messageList) {
					logger.error(TAG, "Remote command AS400 run with error: " + message.getID() + " - " + message.getText());
				}
			} else {
				logger.debug(TAG, "Remote command AS400 ended at: " + sdf.format(new Date()));
	 		}
			as400.disconnectService(AS400.COMMAND);

		} catch (Exception e) {
			logger.error(TAG, "Remote command AS400 run with error: " + e );
		} finally {
			try {
				if (as400 != null) {
					as400.disconnectAllServices();
				}
			} catch (Exception e) {
				logger.error(TAG, "Remote command AS400 run with error: " + e );
			}
		}
	}


}
