package com.progettoedp.testo;

public class Testo {
	
	public static final String SQLITE = "SQLITE";
	public static final String MSSQL = "MSSQL";
	public static final String FIREBIRD = "FIREBIRD";
	public static final String MSSQL_EQ = "MSSQL_EQ";
	public static final String AS400 = "AS400";
	
	public static final String FATEL = "FATEL";
	public static final String IX = "IX";
	public static final String REIMPORT = "REIMPORT";
	public static final String MIGRATE = "MIGRATE";
	public static final String LOCAL = "LOCAL";
	public static final String TEST = "TEST";
	public static final String NOT_IMPORTED = "NOT_IMPORTED";
	
	public static final String ATTIVA = "ATTIVA";
	public static final String PASSIVA = "PASSIVA";
	
	public static final String SPACCIO = "VENDITA SPACCIO";
	
	public static final String PDF = "pdf";
	public static final String XML = "xml";
	public static final String ZIP = "zip";
	
	public static final String ORDINE = "ORDINE";
	public static final String ORDINE_NSO = "NSO";
	
	
	public static final String SI = "SI";
	
	public static final String datiTrasmissione = "DatiTrasmissione";
	public static final String cedentePrestatore = "CedentePrestatore";
	public static final String cessionarioCommittente = "CessionarioCommittente";
	public static final String datiGeneraliDocumento = "DatiGeneraliDocumento";
	public static final String dettaglioLinee = "DettaglioLinee";
	public static final String datiPagamento = "DatiPagamento";
	public static final String importoPagamento = "ImportoPagamento";
	public static final String dataScadenzaPagamento = "DataScadenzaPagamento";
	public static final String modalitaPagamento = "ModalitaPagamento";
	
	public static final String nome = "Nome";
	public static final String cognome = "Cognome";
	
	public static final String codiceValore = "CodiceValore";
	public static final String quantita = "Quantita";
	public static final String prezzoUnitario = "PrezzoUnitario";
	public static final String prezzoTotale = "PrezzoTotale";
	public static final String riga = "NumeroLinea";
	 
	public static final String idCodice = "IdCodice";
	public static final String idPaese = "IdPaese";
	public static final String codiceFiscale = "CodiceFiscale";
	public static final String denominazione = "Denominazione";
 
	 
	public static final String numero = "Numero";
	public static final String data = "Data";
	public static final String tipoDocumento = "TipoDocumento";
	public static final String importoTotaleDocumento = "ImportoTotaleDocumento";

	public static final String causale = "Causale";
	public static final String bolloVirtuale = "BolloVirtuale";
	public static final String importoBollo = "ImportoBollo";
	
	public static final String descrizione = "Descrizione";
	public static final String condizioniPagamento = "CondizioniPagamento";
	
	public static final String imponibileImporto = "ImponibileImporto";
	public static final String aliquotaIVA = "AliquotaIVA";
	public static final String natura = "Natura";
	public static final String esigibilita = "EsigibilitaIVA";
	public static final String imposta = "Imposta";
	public static final String datiRiepilogo = "DatiRiepilogo";
	
	public static final String allegati = "Allegati";
	public static final String attachment = "Attachment";
	public static final String nomeAttachment = "NomeAttachment";
	public static final String formatoAttachment = "FormatoAttachment";
	public static final String compressioneAttachment = "AlgoritmoCompressione";
	
	public static final String IMPORTATO = "IMPORTED";
	public static final String NON_IMPORTATO = "NOT IMPORTED";
	public static final String NON_DEFINITO = "NON DEFINITO";
	
	public static final String IP_AS400 = "192.168.50.1";
	public static final String LIB_ITACA = "IT_SOL_F";
 	public static final String USER_AS400 = "ITACA";
	public static final String PWD_AS400 = "ACATI";
	
	public static final String LIB_AS400 = "IT_V02_O";
	public static final String GA_AS400 = "GA_V01_O";
	public static final String CMD_AS400_1 = "FE0001";
	public static final String CMD_AS400_2 = "FE0002";
	public static final String CMD_AS400_3 = "FE0003";
	public static final String CMD_AS400_4 = "FE0004";
	public static final String USER_ITACA_AS400 = "ITACA";
	
	public static final String FATTUREB2B = "FATTUREB2B";
	public static final String ITACA = "ITACA";
	public static final String EUROQUOTE = "EUROQUOTE";
	
	public static final String CONFIG_MODE = "CONFIG";
	public static final String SOTTOCONTO_FORNITORE = "000030";
	
	public static final String datiOrder = "ns4:Order";
	public static final String ID = "ID";
	public static final String companyID = "CompanyID";
	public static final String issueDate = "IssueDate";
	public static final String note = "Note";
	public static final String name = "Name";
	public static final String cityName = "CityName";
	public static final String buyerCustomerParty = "ns3:BuyerCustomerParty";
	public static final String deliveryLocation = "ns3:Delivery";
	
	public static final String DESTRA = "DX";
	public static final String SINISTRA = "SX";
	
	public static final String NATURA_BOLLO = "N1";
	public static final String NATURA_BOLLO_AGENTE = "N2.2";
	public static final Double IMPORTO_BOLLO = 2.00;
	 
}
