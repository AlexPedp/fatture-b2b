package com.progettoedp.status;

public enum Status {
	
    D("DA VALIDARE"),
    V("VALIDATA"),
    E("GENERATA"),
    F("FIRMATA"),
    I("INVIATA"),
    S("SCARTATA"),
    C("CONSEGNATA"),
    M("MANCATA CONSEGNA"),
    N("NON RECAPITABILI"),
    A("ACCETTATA"),
    R("RIFIUTATA"),
    T("DECORSI TERMINI"),
    O("CONSERVAZIONE SOSTITUTIVA"),
	L("FATTURA PASSIVA DA CONTABILIZZARE");
    

    private String value;

    Status(String value) {
        this.value = value;
    }

    public String getSValue() {
        return this.value;
    }
}
