package com.progettoedp.logic;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.UUID;

import com.progettoedp.dao.DAO;
import com.progettoedp.data.Fattura;
import com.progettoedp.logic.Import.TYPE;
import com.progettoedp.main.Application;
import com.progettoedp.testo.Testo;
import com.progettoedp.util.Logger;

public class NotImported {

	private DAO dao;
	private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	private static final Logger logger = Logger.getLogger(NotImported.class);
	private static final String TAG = "Import";
	private LinkedHashMap<TYPE, File> appFiles = new LinkedHashMap<TYPE, File>();
	private String type; 
 

	public NotImported(DAO dao, String type){
		this.dao = dao;
		this.type=type;
	}
 
	public void run() throws Exception{

		try {

			List<Fattura> fattureAttive = dao.findfattureByTipoAndStato(Testo.ATTIVA, Testo.NON_IMPORTATO, false, type);

			if (!fattureAttive.isEmpty()){

				logger.info(TAG, "Inizio scrittura su AS400 fatture attive Size: " + fattureAttive.size());
				String uuid = UUID.randomUUID().toString().substring(0,8);
				int row = 0;
				for (Fattura f: fattureAttive){
					row++;
					Application.getInstance().getDao().insertDataAs400(f, uuid, row);
					logger.info(TAG, "Fattura " + f.getNomeXml() + "|"  + f.getNumero() + "|" + f.getTipo() + "|" + f.getStatoSdi());
				}
				if (!fattureAttive.isEmpty()){
					Application.getInstance().getDao().insertCmdAs400(uuid, Testo.CMD_AS400_2);
				}

				dao.updateFattureByTipoAndStato(Testo.ATTIVA, Testo.NON_IMPORTATO);
				logger.info(TAG, "Fine scrittura su AS400 fatture attive");
			}


		} catch (Exception e) {
			logger.error(TAG, e);
		}


	}


}
