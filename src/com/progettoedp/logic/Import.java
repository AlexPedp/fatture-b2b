package com.progettoedp.logic;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.EnumUtils;

import com.progettoedp.dao.DAO;
import com.progettoedp.data.Euroquote;
import com.progettoedp.data.Fattura;
import com.progettoedp.data.Fornitore;
import com.progettoedp.data.Ordine;
import com.progettoedp.data.Registrazione;
import com.progettoedp.data.Sdi;
import com.progettoedp.main.Application;
import com.progettoedp.model.XMLModel;
import com.progettoedp.parser.XMLParser;
import com.progettoedp.parser.XMLParserOrder;
import com.progettoedp.receipt.RemoteCommand;
import com.progettoedp.receipt.XMLReceipt;
import com.progettoedp.status.Status;
import com.progettoedp.testo.Testo;
import com.progettoedp.util.Logger;

public class Import {

	private DAO dao;
	private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	private SimpleDateFormat sdfOrder = new SimpleDateFormat("yyyyMM");
	private static final Logger logger = Logger.getLogger(Import.class);
	private static final String TAG = "Import";
	private LinkedHashMap<TYPE, File> appFiles = new LinkedHashMap<TYPE, File>();
	private TYPE type; 
	private String printerName = Application.getInstance().getPrinterName();
	private List<Fattura> fatturePassive = new ArrayList<Fattura>();
	private List<Fattura> fattureAttive = new ArrayList<Fattura>();
	private List<Fattura> fattureSpaccio = new ArrayList<Fattura>();
	private List<Ordine> orders = new ArrayList<Ordine>();
	private String partitaIva;
		
	public Import(DAO dao){
		this.dao = dao;
	}

	public void createFiles(Fattura fattura){
		//		String id = fattura.getNazione() + fattura.getPartitaIva() + "_" + fattura.getNumero().replaceAll("/", "_");

		String id = null;
		if (fattura.getRagioneSociale() != null){
			String ragioneSociale =  fattura.getRagioneSociale().replaceAll("\\.", "").replaceAll(" ", "_").replaceAll("\"", "").replaceAll("/", "").replaceAll("\\n", "").replaceAll("\\r", "").replaceAll("\\\\", "_");
			ragioneSociale =  ragioneSociale.replaceAll("(\\W|^_)*", "");
			id = ragioneSociale + "_" + fattura.getNumero().replaceAll("/", "_").replaceAll(":", "").replace(" ","").replaceAll("\\\\", "_");
		} else if (fattura.getNome() != null && fattura.getCognome() != null){
			id = fattura.getNome() + "_" + fattura.getCognome() + "_" + fattura.getNumero().replaceAll("/", "_").replaceAll("\"", "").replaceAll("\\\\", "_");
		} else {
			id = fattura.getNumero().replaceAll("/", "_").replace("\\\\", "_");
		}

		addFile(id +".html", TYPE.HTML);
		addFile(id +".pdf", TYPE.PDF);
		addFile(id +"_all.pdf", TYPE.ALL);
		addFile(id +"_allegato.pdf", TYPE.ATTACHMENT);
		addFile(id +"_allegato.p7m", TYPE.FIRMA);
		addFile(id +"_allegato.xlsx", TYPE.ATTACHMENT_XLSX);
		addFile(id +"_allegato.xls", TYPE.ATTACHMENT_XLS);
		addFile(id +"_allegato.txt", TYPE.ATTACHMENT_TXT);
		addFile(id +"_allegato.zip", TYPE.ATTACHMENT_ZIP);
		addFile(id +"_allegato.csv", TYPE.ATTACHMENT_CSV);
		addFile(id +"_allegato.xml", TYPE.ATTACHMENT_XML);
		addFile(id +"_allegato_1.pdf", TYPE.ATTACHMENT_PDF_1);
		addFile(id +"_allegato_2.pdf", TYPE.ATTACHMENT_PDF_2);
		addFile(id +"_allegato_3.pdf", TYPE.ATTACHMENT_PDF_3);
		addFile("temp_" + id +".html", TYPE.TEMP);
	}

	public void addFile(String name, TYPE type){
		appFiles.put(type, new File(name));
	}

	public void run() throws Exception{

		String xmlBasePath = Application.getInstance().getXmlPath();
		String pdfBasePath = Application.getInstance().getPdfPath();
		String attachBasePath = Application.getInstance().getAttachPath();
		boolean activeInvoice = Application.getInstance().isActiveInvoice();
		String partitaIva = Application.getInstance().getPartitaIva();
		String xmlAs400Path = Application.getInstance().getXmlAs400Path();
		String tipoProtocollo = Application.getInstance().getTipoProtocollo();
		String[] filteredDocuments = Application.getInstance().getFilteredDocuments();

		try {

			List<Sdi> readDataSdi =  new ArrayList<Sdi>();
			List<Sdi> readDataSdiActive = new ArrayList<Sdi>();
		
			readDataSdi = dao.readDataSdi();
			logger.info(TAG, "Query passive invoice: please wait.............................."+ "|" + readDataSdi.size());

			if (activeInvoice){
				readDataSdiActive = dao.readDataSdiActive();
				logger.info(TAG, "Query active invoice: please wait.............................."+"|" + readDataSdiActive.size());
				readDataSdi.addAll(readDataSdiActive);
			}

			if (readDataSdi.isEmpty())
				logger.error(TAG, "Nessun dato SDI letto");

			int size = readDataSdi.size();
			int read = 0;
			boolean isFirstActive=true;
			logger.info(TAG, "Start reading invoice: " + size + " please wait..............................");
			for(Sdi s: readDataSdi){
				read++;
				logger.debug(TAG, "Count read: " + read);
				
				if (s.getTipo().equals(Testo.PASSIVA)) {
					if (read==1) {
						logger.info(TAG, "Inizio passive: " + read);	
					}
				}
			
				if (s.getTipo().equals(Testo.ATTIVA)) {
					if (isFirstActive) {
						logger.info(TAG, "Fine passive: " + read);
						logger.info(TAG, "Inizio attive: " + read);	
					}
					if (read==readDataSdiActive.size()) {
						logger.info(TAG, "Fine attive: " + read);	
					}
					isFirstActive = false;
				}
			
				String nomeXml = s.getNomeXml();
				String dataRitiro = s.getDataRitiro();
				String dataSdi1 = s.getDataSdi1();
				String dataSdi2 = s.getDataSdi2();
				String dataDoc = s.getDataDoc();
				String numDoc = s.getNumeroDoc();
				String partitaIvaDoc = s.getPartitaIvaDoc();
				String partitaIvaCli = s.getPartitaIvaCli();
				String idSdi = s.getIdSdi();
				String dataRic = s.getDataRicezione();
				String dataCon = s.getDataConsegna();
				String tipo = s.getTipo();
				String statoSdi = s.getStato();
				String tipoDocumento = s.getTipoDocumento();
				String regFiscale = s.getRegFiscale();
				
				if (ArrayUtils.contains(filteredDocuments, tipoDocumento)) {
					logger.info(TAG, "Fattura SDI scartata: " +  partitaIvaCli +  "|" + tipoDocumento + "|" + dataDoc + "|" + numDoc + "|" + nomeXml);
					continue;
				}

				String statoFattura = null;
				if(statoSdi != null){
					if(EnumUtils.isValidEnum(Status.class, statoSdi)){
						Status status = Status.valueOf(statoSdi);
						statoFattura = status.getSValue();	
					} else {
						statoFattura = statoSdi;
					}
				} else {
					statoFattura = "";
				}

				logger.debug(TAG, "Stato fattura: " + statoFattura);

				Date ritiro = null;
				if (dataSdi2 !=null) {
					ritiro = sdf.parse(dataSdi2);					
				} else if  (dataRitiro !=null){
					ritiro = sdf.parse(dataRitiro);
				}


				Calendar cal = Calendar.getInstance();
				cal.setTime(ritiro);
				String path =  cal.get(Calendar.YEAR) + String.format("%02d", cal.get(Calendar.MONTH)+1);
				String secondPath =  cal.get(Calendar.YEAR) + String.format("%02d", cal.get(Calendar.MONTH));
				String thirdPath =  cal.get(Calendar.YEAR) + String.format("%02d", cal.get(Calendar.MONTH)-1);
				String forthPath =  cal.get(Calendar.YEAR) + String.format("%02d", cal.get(Calendar.MONTH)+2);
				logger.debug(TAG, "XML path: " + "|" + path + "|" + secondPath + "|" + thirdPath +  "|" + forthPath);

				File xmlFile = new File(xmlBasePath + "\\" + path + "\\" + nomeXml);
				if (!xmlFile.exists()){
					xmlFile = new File(xmlBasePath + "\\" + secondPath + "\\" + nomeXml);
					if (!xmlFile.exists()){
						xmlFile = new File(xmlBasePath + "\\" + thirdPath + "\\" + nomeXml); 
						if (!xmlFile.exists()){
							xmlFile = new File(xmlBasePath + "\\" + forthPath + "\\" + nomeXml); 
						}
					}
				}

				if (!xmlFile.exists()){
					Date dataDocumento = sdf.parse(dataDoc);	
					cal = Calendar.getInstance();
					cal.setTime(dataDocumento);
					String docPath =  cal.get(Calendar.YEAR) + String.format("%02d", cal.get(Calendar.MONTH)+1);
					xmlFile = new File(xmlBasePath + "\\" + docPath + "\\" + nomeXml);
					if (!xmlFile.exists()){
						cal.add(Calendar.MONTH, -1);
						String docPath1 =  cal.get(Calendar.YEAR) + String.format("%02d", cal.get(Calendar.MONTH)+1);	
						xmlFile = new File(xmlBasePath + "\\" + docPath1 + "\\" + nomeXml); 
					}
				}


				if (xmlFile.exists()){

					logger.debug(TAG, "Reading fattura: " + dataDoc + "|" + numDoc + "|" + xmlFile.toString());

					XMLParser xmlParser = new XMLParser(xmlFile);
					Fattura fattura = xmlParser.run();
					if (fattura==null) {
						continue;
					}
					String causale = null;
					if(fattura.getCausale() == null){
						causale = Testo.NON_DEFINITO;
					} else {
						if  (fattura.getCausale().trim().length() > 50){
							causale = fattura.getCausale().substring(0, 50);
						} else {
							causale = fattura.getCausale();
						}
					}
					fattura.setDataRitiro(dataRitiro);
					fattura.setDataSdi1(dataSdi1);
					fattura.setDataSdi2(dataSdi2);
					fattura.setDataCon(dataCon);
					fattura.setDataRic(dataRic);
					fattura.setStatoSdi(statoFattura);
					fattura.setRegFiscale(regFiscale);
					if (fattura.getPartitaIva() == null){
						fattura.setPartitaIva(partitaIvaDoc);		
					}
					fattura.setIdSdi(idSdi);
					fattura.setNomeXml(nomeXml);
					
					if (causale!=null && causale.equals(Testo.SPACCIO)){
						activeInvoice=true;
					}

					if (fattura.getTipo().equals(Testo.ATTIVA) && !fattura.getPartitaIva().trim().equals(partitaIva.trim())){
						fattura.setTipo(Testo.PASSIVA);
						logger.info(TAG, "Fattura cambia stato: " + read + "|" + size + "|"  + fattura.getPartitaIva() + "|" + fattura.getTipo() +  fattura.getPartitaIva());
					}

					createFiles(fattura); 

					Fattura findFatturaByNumero = dao.findFatturaByNumero(fattura.getTipo(), fattura.getTipoDocumento(), fattura.getData(), fattura.getNumero(), fattura.getPartitaIva());
					if (findFatturaByNumero == null) {

						if (activeInvoice && fattura.getTipo().equals(Testo.ATTIVA)){
							if (causale.equals(Testo.SPACCIO)){
								fattureSpaccio.add(fattura);
							} else {
								fattureAttive.add(fattura);	
							}
							dao.insertFattura(fattura);
							logger.info(TAG, "Fattura inserted: "+ read + "|"  + size + "|"  + fattura.getPartitaIva() +  "|" + fattura.getTipoDocumento() + "|" + fattura.getData() + "|" + fattura.getNumero() + "|" + fattura.getNomeXml());
						} else {


							if (tipoProtocollo.equals(Testo.EUROQUOTE)){
								Euroquote euroquote = dao.readEuroquote(fattura.getNumero(), fattura.getData(), fattura.getPartitaIva());
								if (euroquote != null) {
									fattura.setNumProEq(euroquote.getProtocollo());
									fattura.setDataProEq(euroquote.getDataProtocollo());
								}  	
							}


							Fornitore fornitore = dao.readFornitore(fattura.getPartitaIva(), fattura.getCodiceFiscale());
							if (fornitore!= null){
								if (fornitore.getDescrizione()!= null){
									fattura.setCodice(fornitore.getCodice());
									fattura.setRagioneSocialeGestionale(fornitore.getRagioneSociale());
									path = path + "\\" + fornitore.getDescrizione().trim();
								}
							}


							if (xmlAs400Path != null){
								XMLReceipt receipt = new XMLReceipt(xmlFile, fattura);
								File receiptFile = receipt.run();
								try {
									FileUtils.copyFileToDirectory(xmlFile, new File(xmlAs400Path));
									FileUtils.copyFileToDirectory(receiptFile, new File(xmlAs400Path));
									receiptFile.delete();
								} catch(Exception e) {
									logger.error(TAG, "Errore in creazione ricevuta XML: " + e);
								}

								try {
									String host = Application.getInstance().getHost_as400();
									String uid = Application.getInstance().getUser_as400();
									String pwd = Application.getInstance().getPassword_as400();
									RemoteCommand remoteCommand = new RemoteCommand(host, uid, pwd);
									remoteCommand.run();

								} catch (Exception e) {
									logger.error(TAG, "Errore in remote command AS400: " + e);
								}

							}

							if (tipoProtocollo.equals(Testo.ITACA)){
								Registrazione registrazione = dao.readProtocollo(fattura.getIdSdi());
								if (registrazione!= null){
									if (registrazione.getNumeroProtocollo() != null){

										String dataAccettazione = null;
										if (fattura.getDataRitiro()!= null){
											dataAccettazione = fattura.getDataRitiro();
										} else {
											dataAccettazione = fattura.getDataSdi2();
										}

										fattura.setNumProEq(registrazione.getNumeroProtocollo());
										fattura.setDataProEq(dataAccettazione);
										fattura.setRegistroIva(registrazione.getRegistroIva());
										fattura.setDataSdi1(dataSdi1);
										fattura.setDataRegistrazione(registrazione.getDataRegistrazione());

										logger.info(TAG, "Numero protocollo recuperato correttamente: " + fattura.getNumProEq() + "|" + fattura.getDataProEq());

										if (fornitore != null && fornitore.getSottoconto()>0) {
											int sottoconto = Integer.parseInt(Testo.SOTTOCONTO_FORNITORE);
											if (fornitore.getSottoconto()==sottoconto){
												boolean result = dao.updateEuroquote(fattura.getNumero(), fattura.getData(), fattura.getPartitaIva(), fattura.getNumProEq(), fattura.getDataProEq());
											}
										}

									}
								}	
							}

							fatturePassive.add(fattura);
							dao.insertFattura(fattura);

							logger.info(TAG, "Fattura inserted: " + read + "|" +   size + "|"  + fattura.getPartitaIva() +  "|" + fattura.getTipoDocumento() + "|" + fattura.getData() + "|" + fattura.getNumero() + "|" + fattura.getNomeXml() + "|" + fattura.getIdSdi() + "|" + fattura.getNumProEq());
							XMLModel xmlModel = new XMLModel(xmlFile, fattura, appFiles);
							List<File> files  = xmlModel.run();
							File pdfFile = null;
							File attachFile = null;
							File newPdfFile = null;
							File newAttachFile = null;
							List<File> newAttachFiles = new ArrayList<File>();
							if (files !=null){
								pdfFile = files.get(0);
								newPdfFile = new File(pdfBasePath + "\\" + path + "\\" + pdfFile.getName());
								
								for(int i = 0; i < files.size(); i++){
									if (i>=1){
										attachFile = files.get(i);
										newAttachFile = new File(attachBasePath + "\\" + path + "\\" + attachFile.getName());
										newAttachFiles.add(newAttachFile);	
									}
								}
								 
								deleteFiles(newPdfFile, newAttachFiles);
								 
								if(printerName != null){
									PrintPdf printPdf = new PrintPdf();
									printPdf.print(newPdfFile, printerName);
								}
							}
						}
					} else {

						if (findFatturaByNumero.getStatoSdi() != null){
							logger.debug(TAG, "Stato " + findFatturaByNumero.getStatoSdi());

							String oldStatus = findFatturaByNumero.getStatoSdi().trim();
							String statoFatturaOld=null;
							if(oldStatus != null){
								if(EnumUtils.isValidEnum(Status.class, oldStatus)){
									Status status = Status.valueOf(oldStatus);
									statoFatturaOld = status.getSValue();	
								} else {
									statoFatturaOld = oldStatus;
								}
							} else {
								statoFatturaOld = "";
							}
							
 							if(statoFatturaOld != null && !statoFatturaOld.equals(statoFattura.trim())){
								logger.info(TAG, "Status SDI changes: old: " +  statoFatturaOld + "|" + "new: " + statoFattura + "|" + fattura.getPartitaIva() +  "|" + fattura.getTipoDocumento() + "|" + fattura.getData() + "|" + fattura.getNumero() + "|" + fattura.getNomeXml());

								if (activeInvoice && findFatturaByNumero.getTipo().equals(Testo.ATTIVA)){
									if (causale.equals(Testo.SPACCIO)){
										fattureSpaccio.add(fattura);
									} else {
										fattureAttive.add(fattura);	
									}
									dao.updateFattura(fattura, statoFattura);
								} else{

								}

							} else {
								logger.debug(TAG, "Fattura already imported: " +  read + "|" + size + "|"   + fattura.getPartitaIva() +  "|" + fattura.getTipoDocumento() + "|" + fattura.getData() + "|" + fattura.getNumero() + "|" + fattura.getNomeXml());	
							} 
						} else {

							if (statoFattura.length()>0){
								if (activeInvoice && findFatturaByNumero.getTipo().equals(Testo.ATTIVA)){
									if (causale.equals(Testo.SPACCIO)){
										fattureSpaccio.add(fattura);
									} else {
										fattureAttive.add(fattura);	
									}
									dao.updateFattura(fattura, statoFattura);
									logger.info(TAG, "Status SDI changes: " + "new: " + statoFattura + "|" + fattura.getPartitaIva() +  "|" + fattura.getTipoDocumento() + "|" + fattura.getData() + "|" + fattura.getNumero() + "|" + fattura.getNomeXml());
								}
							}
						}

					}


				} else {
					logger.info(TAG, "XML not found: " +  read + "|" + size + "|"  + xmlFile.toString() + "|" + numDoc + "|" + dataDoc);
				}

			}

			logger.info(TAG, "End reading invoice: please wait..............................");
			executeRemote();


		} catch (Exception e) {
			logger.error(TAG, "Error: " + e);
		}


	}

	public void runLocal() throws Exception{

		String xmlBasePath = Application.getInstance().getXmlPath();
		String pdfBasePath = Application.getInstance().getPdfPath();
		String attachBasePath = Application.getInstance().getAttachPath();
		String partitaIva = Application.getInstance().getPartitaIva();
		String xmlAs400Path = Application.getInstance().getXmlAs400Path();
		String tipoProtocollo = Application.getInstance().getTipoProtocollo();
		String[] filteredDocuments = Application.getInstance().getFilteredDocuments();
		boolean activeInvoice = false;
		String statoFattura = null;

		String currentDir =  System.getProperty("user.dir");
		logger.debug(TAG, "Current dir: " + currentDir);
		File dir = new File(".");
		File finder[] = dir.listFiles(new FileExtensionFilter("xml"));

		for (File xmlFile: finder){
			
			if (xmlFile.isDirectory()){
				continue;
			}

			if (xmlFile.exists()){
				logger.debug(TAG, "XML Files: " + xmlFile.getAbsolutePath());
				XMLParser xmlParser = new XMLParser(xmlFile);
				Fattura fattura = xmlParser.run();
				String causale = null;
				if(fattura.getCausale() == null){
					causale = Testo.NON_DEFINITO;
				} else {
					if  (fattura.getCausale().trim().length() > 50){
						causale = fattura.getCausale().substring(0, 50);
					} else {
						causale = fattura.getCausale();
					}
				}
				String numero = fattura.getNumero();
				String partitaIvaXml = fattura.getPartitaIvaCliente();
				String codiceFiscaleCli = fattura.getCodiceFiscaleCliente();
				
				if (causale!=null && causale.equals(Testo.SPACCIO)){
					activeInvoice=true;
				}
				
				logger.info(TAG, "Reading fattura: " + fattura.getData() + "|" + fattura.getNumero() + "|" + xmlFile.toString());

				if (fattura.getTipo().equals(Testo.ATTIVA) && !fattura.getPartitaIva().trim().equals(partitaIva.trim())){
					fattura.setTipo(Testo.PASSIVA);
					logger.info(TAG, "Fattura cambia stato: " + "|"  + fattura.getPartitaIva() + "|" + fattura.getTipo() +  fattura.getPartitaIva());
				}

				createFiles(fattura); 

				dao.deleteFatturaByNumero(fattura.getTipo(), fattura.getTipoDocumento(), fattura.getData(), fattura.getNumero(), fattura.getPartitaIva());

				Fattura findFatturaByNumero = dao.findFatturaByNumero(fattura.getTipo(), fattura.getTipoDocumento(), fattura.getData(), fattura.getNumero(), fattura.getPartitaIva());
				if (findFatturaByNumero == null) {
				
					Sdi sdi = dao.readDataSdiByNumero(fattura.getTipoDocumento(), fattura.getData(), fattura.getNumero(), fattura.getPartitaIva());
					String nomeXml = sdi.getNomeXml();
					String dataDoc = sdi.getDataDoc();
					String numDoc = sdi.getNumeroDoc();
					String partitaIvaDoc = sdi.getPartitaIvaDoc();
					String partitaIvaCli = sdi.getPartitaIvaCli();
					String statoSdi = sdi.getStato();
				 
					if(statoSdi != null){
						if(EnumUtils.isValidEnum(Status.class, statoSdi)){
							Status status = Status.valueOf(statoSdi);
							statoFattura = status.getSValue();	
						} else {
							statoFattura = statoSdi;
						}
					} else {
						statoFattura = "";
					}
					
					
					String tipoDocumento = sdi.getTipoDocumento();
					String regFiscale = sdi.getRegFiscale();
					
					if (ArrayUtils.contains(filteredDocuments, tipoDocumento)) {
						logger.info(TAG, "Fattura SDI scartata: " +  partitaIvaCli +  "|" + tipoDocumento + "|" + dataDoc + "|" + numDoc + "|" + nomeXml);
						continue;
					}


					fattura.setDataRitiro(sdi.getDataRitiro());
					fattura.setDataSdi1(sdi.getDataSdi1());
					fattura.setDataSdi2(sdi.getDataSdi2());
					fattura.setDataCon(sdi.getDataConsegna());
					fattura.setDataRic(sdi.getDataRicezione());
					fattura.setStatoSdi(statoFattura);
					fattura.setRegFiscale(regFiscale);
					if (fattura.getPartitaIva() == null){
						fattura.setPartitaIva(sdi.getPartitaIvaDoc());		
					}
					fattura.setIdSdi(sdi.getIdSdi());
					fattura.setNomeXml(sdi.getNomeXml());

					Date ritiro = null;
					if (sdi.getDataSdi2() !=null) {
						ritiro = sdf.parse(sdi.getDataSdi2());					
					} else if  (sdi.getDataRitiro() !=null){
						ritiro = sdf.parse(sdi.getDataRitiro());
					}
					
					if (activeInvoice) {
						if(ritiro == null){
							ritiro = new Date();
					    }
						if (fattura.getNumero()==null){
							fattura.setNumero(numero);
						}
						if (fattura.getIdSdi()==null){
							fattura.setIdSdi(numero);
						}
						if (fattura.getCodiceFiscaleCliente()==null){
							fattura.setCodiceFiscaleCliente(codiceFiscaleCli);
						}
						if (fattura.getPartitaIvaCliente()==null){
							fattura.setPartitaIvaCliente(partitaIvaXml);
						}
						if (fattura.getDataRitiro()==null){
							fattura.setDataRitiro(sdf.format(new Date()));
						}
					}
					
					Calendar cal = Calendar.getInstance();
					cal.setTime(ritiro);
					String path =  cal.get(Calendar.YEAR) + String.format("%02d", cal.get(Calendar.MONTH)+1);
 

					logger.info(TAG, "Stato fattura: " + statoFattura + " Causale " + causale);

					if (activeInvoice && fattura.getTipo().equals(Testo.ATTIVA)){
						if (causale!=null && causale.equals(Testo.SPACCIO)){
							fattureSpaccio.add(fattura);
						} else {
							fattureAttive.add(fattura);	
						}
						dao.insertFattura(fattura);
						logger.info(TAG, "Fattura inserted:" + fattura.getPartitaIva() +  "|" + fattura.getTipoDocumento() + "|" + fattura.getData() + "|" + fattura.getNumero() + "|" + fattura.getNomeXml());
					} else {


						if (tipoProtocollo.equals(Testo.EUROQUOTE)){
							Euroquote euroquote = dao.readEuroquote(fattura.getNumero(), fattura.getData(), fattura.getPartitaIva());
							if (euroquote != null) {
								fattura.setNumProEq(euroquote.getProtocollo());
								fattura.setDataProEq(euroquote.getDataProtocollo());
							}  	
						}


						Fornitore fornitore = dao.readFornitore(fattura.getPartitaIva(), fattura.getCodiceFiscale());
						if (fornitore!= null){
							if (fornitore.getDescrizione()!= null){
								fattura.setCodice(fornitore.getCodice());
								fattura.setRagioneSocialeGestionale(fornitore.getRagioneSociale());
								path = path + "\\" + fornitore.getDescrizione().trim();
							}
						}


						if (xmlAs400Path != null){
							XMLReceipt receipt = new XMLReceipt(xmlFile, fattura);
							File receiptFile = receipt.run();
							try {
								FileUtils.copyFileToDirectory(xmlFile, new File(xmlAs400Path));
								FileUtils.copyFileToDirectory(receiptFile, new File(xmlAs400Path));
								receiptFile.delete();
							} catch(Exception e) {
								logger.error(TAG, "Errore in creazione ricevuta XML: " + e);
							}

							try {
								String host = Application.getInstance().getHost_as400();
								String uid = Application.getInstance().getUser_as400();
								String pwd = Application.getInstance().getPassword_as400();
								RemoteCommand remoteCommand = new RemoteCommand(host, uid, pwd);
								remoteCommand.run();

							} catch (Exception e) {
								logger.error(TAG, "Errore in remote command AS400: " + e);
							}

						}

						if (tipoProtocollo.equals(Testo.ITACA)){
							Registrazione registrazione = dao.readProtocollo(fattura.getIdSdi());
							if (registrazione!= null){
								if (registrazione.getNumeroProtocollo() != null){

									String dataAccettazione = null;
									if (fattura.getDataRitiro()!= null){
										dataAccettazione = fattura.getDataRitiro();
									} else {
										dataAccettazione = fattura.getDataSdi2();
									}

									fattura.setNumProEq(registrazione.getNumeroProtocollo());
									fattura.setDataProEq(dataAccettazione);
									fattura.setRegistroIva(registrazione.getRegistroIva());
									fattura.setDataRegistrazione(registrazione.getDataRegistrazione());

									logger.info(TAG, "Numero protocollo recuperato correttamente: " + fattura.getNumProEq() + "|" + fattura.getDataProEq());

									if (fornitore != null && fornitore.getSottoconto()>0) {
										int sottoconto = Integer.parseInt(Testo.SOTTOCONTO_FORNITORE);
										if (fornitore.getSottoconto()==sottoconto){
											boolean result = dao.updateEuroquote(fattura.getNumero(), fattura.getData(), fattura.getPartitaIva(), fattura.getNumProEq(), fattura.getDataProEq());
										}
									}

								}
							}	
						}

						fatturePassive.add(fattura);
						dao.insertFattura(fattura);

						logger.info(TAG, "Fattura inserted: " + fattura.getPartitaIva() +  "|" + fattura.getTipoDocumento() + "|" + fattura.getData() + "|" + fattura.getNumero() + "|" + fattura.getNomeXml() + "|" + fattura.getIdSdi() + "|" + fattura.getNumProEq());
						XMLModel xmlModel = new XMLModel(xmlFile, fattura, appFiles);
						List<File> files  = xmlModel.run();
						File pdfFile = null;
						File attachFile = null;
						File newPdfFile = null;
						File newAttachFile = null;
						List<File> newAttachFiles = new ArrayList<File>();
						if (files !=null){
							pdfFile = files.get(0);
							newPdfFile = new File(pdfBasePath + "\\" + path + "\\" + pdfFile.getName());
							
							for(int i = 0; i < files.size(); i++){
								if (i>=1){
									attachFile = files.get(i);
									newAttachFile = new File(attachBasePath + "\\" + path + "\\" + attachFile.getName());
									newAttachFiles.add(newAttachFile);	
								}
							}
							 
							deleteFiles(newPdfFile, newAttachFiles);

							if(printerName != null){
								PrintPdf printPdf = new PrintPdf();
								printPdf.print(newPdfFile, printerName);
							}
						}
					}
				} else {

					if (findFatturaByNumero.getStatoSdi() != null){
						logger.info(TAG, "Stato " + findFatturaByNumero.getStatoSdi());

						String oldStatus = findFatturaByNumero.getStatoSdi().trim();
						if(oldStatus != null && !oldStatus.equals(statoFattura.trim())){
							logger.info(TAG, "Status SDI changes: old: " +  oldStatus + "|" + "new: " + statoFattura + "|" + fattura.getPartitaIva() +  "|" + fattura.getTipoDocumento() + "|" + fattura.getData() + "|" + fattura.getNumero() + "|" + fattura.getNomeXml());

							if (activeInvoice && findFatturaByNumero.getTipo().equals(Testo.ATTIVA)){
								if (causale.equals(Testo.SPACCIO)){
									fattureSpaccio.add(fattura);
								} else {
									fattureAttive.add(fattura);	
								}
								dao.updateFattura(fattura, statoFattura);
							} else{

							}

						} else {
							logger.info(TAG, "Fattura already imported: "  + fattura.getPartitaIva() +  "|" + fattura.getTipoDocumento() + "|" + fattura.getData() + "|" + fattura.getNumero() + "|" + fattura.getNomeXml());	
						} 
					} else {

						if (statoFattura.length()>0){
							if (activeInvoice && findFatturaByNumero.getTipo().equals(Testo.ATTIVA)){
								if (causale.equals(Testo.SPACCIO)){
									fattureSpaccio.add(fattura);
								} else {
									fattureAttive.add(fattura);	
								}
								dao.updateFattura(fattura, statoFattura);
								logger.info(TAG, "Status SDI changes: " + "new: " + statoFattura + "|" + fattura.getPartitaIva() +  "|" + fattura.getTipoDocumento() + "|" + fattura.getData() + "|" + fattura.getNumero() + "|" + fattura.getNomeXml());
							}
						}
					}

				}



				try {
					xmlFile.delete();
				} catch (Exception e) {
					logger.error(TAG, "XML delete error: "  + e); 
				}

			} else {
				logger.info(TAG, "XML not found: "  + xmlFile.toString());
			}

		}

		logger.info(TAG, "End reading invoice: please wait..............................");
		executeRemote();


	}

	public void runTest() throws Exception{

		String currentDir =  System.getProperty("user.dir");

		//		String xmlBasePath = Application.getInstance().getXmlPath();
		String pdfBasePath = currentDir;
		String xmlAs400Path = Application.getInstance().getXmlAs400Path();
		String attachBasePath = Application.getInstance().getAttachPath();
		String[] filteredDocuments = Application.getInstance().getFilteredDocuments();

		//		String xmlBasePath = Application.getInstance().getXmlPath();
		//		String pdfBasePath = Application.getInstance().getPdfPath();
		//		String attachBasePath = Application.getInstance().getAttachPath();

		List<Fattura> fatture = new ArrayList<Fattura>();

		File dir = new File(".");
		File finder[] = dir.listFiles(new FileExtensionFilter("xml"));

		for (File file: finder){

			if (file.isDirectory()){
				continue;
			}

			XMLParser xmlParser = new XMLParser(file);
			Fattura fattura = xmlParser.run();
			fattura.setDataRitiro("08/04/2021");
			fattura.setDataSdi1("08/04/2021");
			fattura.setDataCon("08/04/2021");
			fattura.setDataSdi2("08/04/2021");
			fattura.setIdSdi("4545454544");
			fattura.setNomeXml(file.getName());
			fatture.add(fattura);
			createFiles(fattura);


			Fattura findFatturaByNumero = dao.findFatturaByNumero(fattura.getTipo(), fattura.getTipoDocumento(), fattura.getData(), fattura.getNumero(), fattura.getPartitaIva());

			//			Euroquote euroquote = dao.readEuroquote(fattura.getNumero(), fattura.getData(), fattura.getPartitaIva());
			//			if (euroquote != null) {
			//				fattura.setNumProEq(euroquote.getProtocollo());
			//				fattura.setDataProEq(euroquote.getDataProtocollo());
			//			}  

			if (findFatturaByNumero == null) {
				fatturePassive.add(fattura);
				dao.insertFattura(fattura);

				//			Fornitore fornitore = dao.readFornitore(fattura.getPartitaIva());
				//			if (fornitore!= null){
				//				if (fornitore.getDescrizione()!= null){
				//
				//				}
				//			}

				logger.info(TAG, "Fattura inserted: " + fattura.getPartitaIva() +  "|" + fattura.getTipoDocumento() + "|" + fattura.getData() + "|" + fattura.getNumero() + "|" + fattura.getNomeXml());
				XMLModel xmlModel = new XMLModel(file, fattura, appFiles);
				List<File> files  = xmlModel.run();
				File pdfFile = null;
				File attachFile = null;
				File newPdfFile = null;
				File newAttachFile = null;
				List<File> newAttachFiles = new ArrayList<File>();
				if (files !=null){
					pdfFile = files.get(0);

					//					PrintPdf printPdf = new PrintPdf();
					//				 	printPdf.print(pdfFile, printerName);

					newPdfFile = new File(pdfBasePath + "/" +  pdfFile.getName());
					for(int i = 0; i < files.size(); i++){
						if (i>=1){
							attachFile = files.get(i);
							newAttachFile = new File(attachBasePath + "/"  + attachFile.getName());
							newAttachFiles.add(newAttachFile);	
						}
					}
					 
					deleteFiles(newPdfFile, newAttachFiles);

					//				if (xmlAs400Path != null){
					//					XMLReceipt receipt = new XMLReceipt(file, fattura);
					//					File receiptFile = receipt.run();
					//					FileUtils.copyFileToDirectory(file, new File(xmlAs400Path));
					//					FileUtils.copyFileToDirectory(receiptFile, new File(xmlAs400Path));
					//					receiptFile.delete();
					//				}

				}
				//			} else {
				//				logger.info(TAG, "Fattura already imported: " +  fattura.getPartitaIva() +  "|" + fattura.getTipoDocumento() + "|" + fattura.getData() + "|" + fattura.getNumero() + "|" + fattura.getNomeXml());
			}
		}

				logger.info(TAG, "End reading invoice: please wait..............................");
//				executeRemote();

	}

	public void runOrder() throws Exception{
		
		String partitaIva = Application.getInstance().getPartitaIva();
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.DATE, -7);
		String annoMese = sdfOrder.format(cal.getTime());

		logger.info(TAG, "Start reading orders: please wait..............................");

		String xmlOrderPath = Application.getInstance().getXmlOrderPath()+"\\"+annoMese;
		String xmlOrderBackupPath = xmlOrderPath + "\\backup";
	
		File dir = new File(xmlOrderPath);
		File finder[] = dir.listFiles(new FileExtensionFilter("xml"));

		for (File file: finder){

			if (file.isDirectory()){
				continue;
			}

			String nomeXml = file.getName();
			
			String xmlFilePath = xmlOrderPath + "\\" + nomeXml;
			String backupFilePath = xmlOrderBackupPath + "\\" +nomeXml;

			
			XMLParserOrder xmlParserOrder = new XMLParserOrder(file);
			Ordine order = xmlParserOrder.run();
			order.setNomeXml(nomeXml);
			
			Ordine findOrdineByNumero = dao.findOrdineByNumero(Testo.ORDINE, Testo.ORDINE, order.getIdOrder(), order.getIssueDate(), partitaIva);

			if (findOrdineByNumero == null) {
				orders.add(order);
				dao.insertOrdine(order);
				logger.info(TAG, "Order inserted: " + order.getIdOrder() +  "|" + order.getIdDelivery() + "|" + order.getName() + "|" + order.getNote());

			} else {
				logger.info(TAG, "Order already imported: " +  order.getIdOrder() +  "|" + order.getNote()  + "|" + order.getIssueDate());
			}
		 	
			try {
				
				File directory = new File(xmlOrderBackupPath);
			    if (! directory.exists()){
			        directory.mkdir();
			    }
				
				 Path move = Files.move 
					        (Paths.get(xmlFilePath),  
					        Paths.get(backupFilePath)); 
			} catch (Exception e) {
				logger.error(TAG, e);
			}
			
		}
		
		logger.info(TAG, "End reading orders: please wait..............................");
		executeRemote();

	}

	public void deleteFiles(File renameFilePdf, List<File> renameFileAttach){

		for (Map.Entry<TYPE, File> item : appFiles.entrySet()) {
			TYPE key = item.getKey();
			File value = item.getValue();

			//			System.out.println(value.getAbsolutePath());
			String path = null; 

			if (value.exists()){
				switch (key) {
				case HTML:
					value.delete();
					break;
				case PDF:
					if (!renameFilePdf.getParentFile().exists()){
						renameFilePdf.getParentFile().mkdirs();
					}
					if (renameFilePdf.exists()){
						renameFilePdf.delete();
					}
					value.renameTo(renameFilePdf);
					break;
				case ATTACHMENT:
				case ATTACHMENT_PDF_1:
				case ATTACHMENT_PDF_2:
				case ATTACHMENT_PDF_3:	
				case ATTACHMENT_TXT:
				case ATTACHMENT_XLSX:
				case ATTACHMENT_XLS:
				case ATTACHMENT_ZIP:
				case ATTACHMENT_CSV:
				case ATTACHMENT_XML:
				case FIRMA:
					
					for (File f: renameFileAttach){
						
						String extension = FilenameUtils.getExtension(f.getAbsolutePath());
						if (!FilenameUtils.getExtension(value.getAbsolutePath().toUpperCase()).equals(extension.toUpperCase())){
							continue;
						}
						
						String name = FilenameUtils.getName(f.getAbsolutePath());
						if (!value.getName().contains(name)) {
							continue;
						}
						
						
						logger.info(TAG, "Rename from " + value.getAbsolutePath() + " to "+ f.getAbsolutePath());
						if (f != null){
							if (!f.getParentFile().exists()){
								f.getParentFile().mkdirs();
							}
							if (f.exists()){
								f.delete();
							}
							value.renameTo(f);
						}
					}
					
				
					break;
				case ALL:
					break;
				case TEMP:
					value.delete();
					break;
				default:
					break;
				}
			}


		}
	}

	public enum TYPE {
		PDF,
		ATTACHMENT,
		ALL,
		HTML,
		TEMP,
		FIRMA,
		ATTACHMENT_XLSX,
		ATTACHMENT_XLS,
		ATTACHMENT_TXT,
		ATTACHMENT_ZIP,
		ATTACHMENT_CSV,
		ATTACHMENT_XML,
		ATTACHMENT_PDF_1,
		ATTACHMENT_PDF_2,
		ATTACHMENT_PDF_3;
	}

	public boolean indexExists(final List list, final int index) {
		return index >= 0 && index < list.size();
	}

	public void executeRemote() {

		try {
			if (!fatturePassive.isEmpty()){

				logger.info(TAG, "Inizio scrittura su AS400 fatture passive Size: " + fatturePassive.size());
				String uuid = UUID.randomUUID().toString().substring(0,8);
				int row = 0;
				for (Fattura f: fatturePassive){
					row++;
					Application.getInstance().getDao().insertDataAs400(f, uuid, row);
				}
				if (!fatturePassive.isEmpty()){
					Application.getInstance().getDao().insertCmdAs400(uuid, Testo.CMD_AS400_1);
				}
				logger.info(TAG, "Fine scrittura su AS400 fatture passive e pulizia archivi");
				fatturePassive.clear();
			}

			if (!fattureAttive.isEmpty()){

				logger.info(TAG, "Inizio scrittura su AS400 fatture attive Size: " + fattureAttive.size());
				String uuid = UUID.randomUUID().toString().substring(0,8);
				int row = 0;
				for (Fattura f: fattureAttive){
					row++;
					Application.getInstance().getDao().insertDataAs400(f, uuid, row);
				}
				if (!fattureAttive.isEmpty()){
					Application.getInstance().getDao().insertCmdAs400(uuid, Testo.CMD_AS400_2);
				}
				logger.info(TAG, "Fine scrittura su AS400 fatture attive e pulizia archivi");
				fattureAttive.clear();
			}
			
			if (!fattureSpaccio.isEmpty()){

				logger.info(TAG, "Inizio scrittura su AS400 fatture spaccio Size: " + fattureSpaccio.size());
				String uuid = UUID.randomUUID().toString().substring(0,8);
				int row = 0;
				for (Fattura f: fattureSpaccio){
					row++;
					Application.getInstance().getDao().insertDataAs400(f, uuid, row);
				}
				if (!fattureSpaccio.isEmpty()){
					Application.getInstance().getDao().insertCmdAs400(uuid, Testo.CMD_AS400_4);
				}
				logger.info(TAG, "Fine scrittura su AS400 fatture spaccio e pulizia archivi");
				fattureSpaccio.clear();
			}

			if (!orders.isEmpty()){

				logger.info(TAG, "Inizio scrittura su AS400 ordini NSO Size: " + orders.size());
				String uuid = UUID.randomUUID().toString().substring(0,8);
				int row = 0;
				for (Ordine o: orders){
					row++;
					Application.getInstance().getDao().insertOrdineAs400(o, uuid, row);
				}
				if (!orders.isEmpty()){
					Application.getInstance().getDao().insertCmdAs400(uuid, Testo.CMD_AS400_3);
				}
				logger.info(TAG, "Fine scrittura su AS400 ordini NSO e pulizia archivi");
				orders.clear();
			}


		} catch (Exception e) {
			logger.error(TAG, e);
		}

	}

}
