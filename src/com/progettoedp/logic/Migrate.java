package com.progettoedp.logic;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import com.progettoedp.dao.DAO;
import com.progettoedp.data.Fattura;
import com.progettoedp.logic.Import.TYPE;
import com.progettoedp.main.Application;
import com.progettoedp.testo.Testo;
import com.progettoedp.util.Logger;

public class Migrate {

	private DAO dao;
	private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	private static final Logger logger = Logger.getLogger(Migrate.class);
	private static final String TAG = "Import";
	private LinkedHashMap<TYPE, File> appFiles = new LinkedHashMap<TYPE, File>();
	private TYPE type; 
	private String printerName = Application.getInstance().getPrinterName();
	private List<Fattura> fatturePassive = new ArrayList<Fattura>();
	private List<Fattura> fattureAttive = new ArrayList<Fattura>();

	public Migrate(DAO dao){
		this.dao = dao;
	}
 
 

	public void run() throws Exception{
		 
		try {
			
			List<Fattura> allFatture = dao.findAllFatture(10000, 0);
			execute(allFatture);
			
			allFatture.clear();
			allFatture = dao.findAllFatture(10000, 10000);
			execute(allFatture);
			
			allFatture.clear();
			allFatture = dao.findAllFatture(10000, 20000);
			execute(allFatture);
			
			allFatture.clear();
			allFatture = dao.findAllFatture(10000, 30000);
			execute(allFatture);
			
			allFatture.clear();
			allFatture = dao.findAllFatture(10000, 40000);
			execute(allFatture);
			
			allFatture.clear();
			allFatture = dao.findAllFatture(10000, 50000);
			execute(allFatture);
			
			allFatture.clear();
			allFatture = dao.findAllFatture(10000, 60000);
			execute(allFatture);
		
			
			} catch (Exception e) {
				logger.error(TAG, e);
			}

	}
	
	public void execute(List<Fattura> allFatture ){
		if (!allFatture.isEmpty()){

			int size = allFatture.size();
			logger.info(TAG, "Inizio migrazione fatture Size: " + size);
			
			int count = 0;
			for (Fattura f: allFatture){
				count++;
//				if (count>10){
//					break;
//				}
				dao.insertFatturaFromMigrate(f, Testo.MSSQL);
				logger.info(TAG, "Scrittura fattura from Sqlite to MSsql: "  + count + '|' + size);	
			}
			
			
			logger.info(TAG, "Fine migrazione fatture");
		}


	}
 

}
