package com.progettoedp.logic;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.UUID;

import com.progettoedp.dao.DAO;
import com.progettoedp.data.Fattura;
import com.progettoedp.logic.Import.TYPE;
import com.progettoedp.main.Application;
import com.progettoedp.testo.Testo;
import com.progettoedp.util.Logger;

public class Reimport {

	private DAO dao;
	private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	private static final Logger logger = Logger.getLogger(Reimport.class);
	private static final String TAG = "Import";
	private LinkedHashMap<TYPE, File> appFiles = new LinkedHashMap<TYPE, File>();
	private String type; 
 

	public Reimport(DAO dao, String type){
		this.dao = dao;
		this.type=type;
	}
 

	public void run() throws Exception{
		 
		try {
			boolean spaccio = false;
			List<Fattura> fattureAttive = dao.findfattureByTipoAndStato(Testo.ATTIVA, Testo.NON_IMPORTATO, spaccio, type);
			List<Fattura> fatturePassive = dao.findfattureByTipoAndStato(Testo.PASSIVA, Testo.NON_IMPORTATO, spaccio, type);
			
			spaccio = true;
			List<Fattura> fattureSpaccio = dao.findfattureByTipoAndStato(Testo.PASSIVA, Testo.NON_IMPORTATO, spaccio, type);
			
			
				if (!fatturePassive.isEmpty()){

					logger.info(TAG, "Inizio scrittura su AS400 fatture passive Size: " + fatturePassive.size());
					String uuid = UUID.randomUUID().toString().substring(0,8);
					int row = 0;
					for (Fattura f: fatturePassive){
						row++;
						Application.getInstance().getDao().insertDataAs400(f, uuid, row);
					}
					if (!fatturePassive.isEmpty()){
						Application.getInstance().getDao().insertCmdAs400(uuid, Testo.CMD_AS400_1);
					}
					
					dao.updateFattureByTipoAndStato(Testo.PASSIVA, Testo.NON_IMPORTATO);
					logger.info(TAG, "Fine scrittura su AS400 fatture passive");
				}

				if (!fattureAttive.isEmpty()){

					logger.info(TAG, "Inizio scrittura su AS400 fatture attive Size: " + fattureAttive.size());
					String uuid = UUID.randomUUID().toString().substring(0,8);
					int row = 0;
					for (Fattura f: fattureAttive){
						row++;
						Application.getInstance().getDao().insertDataAs400(f, uuid, row);
					}
					if (!fattureAttive.isEmpty()){
						Application.getInstance().getDao().insertCmdAs400(uuid, Testo.CMD_AS400_2);
					}
					
					dao.updateFattureByTipoAndStato(Testo.ATTIVA, Testo.NON_IMPORTATO);
					logger.info(TAG, "Fine scrittura su AS400 fatture attive");
				}
				
				if (!fattureSpaccio.isEmpty()){

					logger.info(TAG, "Inizio scrittura su AS400 fatture spaccio Size: " + fattureSpaccio.size());
					String uuid = UUID.randomUUID().toString().substring(0,8);
					int row = 0;
					for (Fattura f: fattureSpaccio){
						row++;
						Application.getInstance().getDao().insertDataAs400(f, uuid, row);
					}
					if (!fattureSpaccio.isEmpty()){
						Application.getInstance().getDao().insertCmdAs400(uuid, Testo.CMD_AS400_4);
					}
					
					dao.updateFattureByTipoAndStato(Testo.ATTIVA, Testo.NON_IMPORTATO);
					logger.info(TAG, "Fine scrittura su AS400 fatture spaccio");
				}


			} catch (Exception e) {
				logger.error(TAG, e);
			}
		 

	}
 

}
