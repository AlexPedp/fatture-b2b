package com.progettoedp.logic;

import java.awt.print.PrinterJob;
import java.io.File;

import javax.print.PrintService;
import javax.print.PrintServiceLookup;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.printing.PDFPageable;
import org.apache.pdfbox.printing.PDFPrintable;
import org.apache.pdfbox.printing.Scaling;
import org.apache.pdfbox.text.PDFTextStripper;
 

public class PrintPdf {
 
	public void print(File file, String printer) {

		try {
			PDDocument document = PDDocument.load(file);

			PDFPageable p = new PDFPageable(document);
			PDFPrintable printable = null;

			PrintService myPrintService = findPrintService(printer);
			PrinterJob job = PrinterJob.getPrinterJob();
			job.setPageable(new PDFPageable(document));
			job.setPrintService(myPrintService);
			
			
			PDPage page = document.getPage(0);

			PDRectangle mediaBox = page.getMediaBox();
			boolean isLandscape = mediaBox.getWidth() > mediaBox.getHeight();
			int rotation = page.getRotation();
			if (rotation == 90 || rotation == 270)
				isLandscape = !isLandscape;
			
			if (isLandscape && isPageToScale(document))  {
				printable = new PDFPrintable(document,Scaling.SCALE_TO_FIT);
				job.setPageable(p);
				job.setPrintable(printable);
			} 
//				else {
//				printable = new PDFPrintable(document,Scaling.ACTUAL_SIZE);
//			}

			
			job.print();
		} catch (Exception e) {
			e.printStackTrace();
		}


	}

	private static PrintService findPrintService(String printerName) {
		PrintService[] printServices = PrintServiceLookup.lookupPrintServices(null, null);

		for (PrintService printService : printServices) {
//			System.out.println(printService.getName());
			if (printService.getName().trim().equals(printerName)) {
				return printService;
			}
		}
		return null;
	}

	public static boolean isPageToScale(PDDocument document){
		boolean isPageToScale = true;
		try {
			String s =  new PDFTextStripper().getText(document);
//			if (s.contains(CARREFOUR) || (s.contains(AUCHAN))){
//				isPageToScale = true;
//				return isPageToScale;
//			}
			 
		} catch (Exception e) {
			isPageToScale = false;
			return isPageToScale;
		}
		return isPageToScale;
	}

}
