package com.progettoedp.data;

import java.util.List;

import com.google.gson.Gson;

public class Fattura {
	
	String codice;
	String ragioneSocialeGestionale;
	String tipo;
	String tipoDocumento;
	Double totaleFattura = 0.0;
	String numero;
	String data;
	String partitaIvaMittente;
	String partitaIva;
	String nazione;
	String ragioneSociale;
	String nome;
	String cognome;
	List<String> nomeAllegato;
	List<String> formatoAllegato;
	List<String> compressioneAllegato;
	List<String> allegato;
	String dataRitiro;
	String dataSdi1;
	String dataSdi2;
	String idSdi;
	String nomeXml;
	String numProEq;
	String dataProEq;
	String dataRic;
	String dataCon;
	String statoSdi;
	String regFiscale;
	String registroIva;
	String codiceFiscale;
	String dataRegistrazione;
	String causale;
	String codiceFiscaleCliente;
	String partitaIvaCliente;
	String bolloVirtuale;
	Double importoBollo=0.0;
	List<Dettaglio> dettagli;
	List<Riepilogo> riepiloghi;
	List<Pagamento> pagamenti;
	
	
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public Double getTotaleFattura() {
		return totaleFattura;
	}
	public void setTotaleFattura(Double totaleFattura) {
		this.totaleFattura = totaleFattura;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getPartitaIvaMittente() {
		return partitaIvaMittente;
	}
	public void setPartitaIvaMittente(String partitaIvaMittente) {
		this.partitaIvaMittente = partitaIvaMittente;
	}
	public String getPartitaIva() {
		return partitaIva;
	}
	public void setPartitaIva(String partitaIva) {
		this.partitaIva = partitaIva;
	}
	public String getRagioneSociale() {
		return ragioneSociale;
	}
	public void setRagioneSociale(String ragioneSociale) {
		this.ragioneSociale = ragioneSociale;
	}
	public List<Dettaglio> getDettagli() {
		return dettagli;
	}
	public void setDettagli(List<Dettaglio> dettagli) {
		this.dettagli = dettagli;
	}
	public List<Riepilogo> getRiepiloghi() {
		return riepiloghi;
	}
	public void setRiepiloghi(List<Riepilogo> riepiloghi) {
		this.riepiloghi = riepiloghi;
	}
	public List<Pagamento> getPagamenti() {
		return pagamenti;
	}
	public void setPagamenti(List<Pagamento> pagamenti) {
		this.pagamenti = pagamenti;
	}
	
	public String getDettagliJson(){
		Gson gson = new Gson();
		return gson.toJson(dettagli);
	}
	
	public String getRiepiloghiJson(){
		Gson gson = new Gson();
		return gson.toJson(riepiloghi);
	}
	
	public String getAllegatiJson(){
		Gson gson = new Gson();
		return gson.toJson(allegato);
	}
	
	public String getPagamentiJson(){
		Gson gson = new Gson();
		return gson.toJson(pagamenti);
	}
	public String getDataRitiro() {
		return dataRitiro;
	}
	public void setDataRitiro(String dataRitiro) {
		this.dataRitiro = dataRitiro;
	}
	public String getDataSdi1() {
		return dataSdi1;
	}
	public void setDataSdi1(String dataSdi1) {
		this.dataSdi1 = dataSdi1;
	}
	public String getDataSdi2() {
		return dataSdi2;
	}
	public void setDataSdi2(String dataSdi2) {
		this.dataSdi2 = dataSdi2;
	}
	public List<String> getAllegato() {
		return allegato;
	}
	public void setAllegato(List<String> allegato) {
		this.allegato = allegato;
	}
	public String getNazione() {
		return nazione;
	}
	public void setNazione(String nazione) {
		this.nazione = nazione;
	}
	public String getIdSdi() {
		return idSdi;
	}
	public void setIdSdi(String idSdi) {
		this.idSdi = idSdi;
	}
	public String getNomeXml() {
		return nomeXml;
	}
	public void setNomeXml(String nomeXml) {
		this.nomeXml = nomeXml;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCognome() {
		return cognome;
	}
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	public String getNumProEq() {
		return numProEq;
	}
	public void setNumProEq(String numProEq) {
		this.numProEq = numProEq;
	}
	public String getDataProEq() {
		return dataProEq;
	}
	public void setDataProEq(String dataProEq) {
		this.dataProEq = dataProEq;
	}
	public String getDataRic() {
		return dataRic;
	}
	public void setDataRic(String dataRic) {
		this.dataRic = dataRic;
	}
	public String getDataCon() {
		return dataCon;
	}
	public void setDataCon(String dataCon) {
		this.dataCon = dataCon;
	}
	public List<String> getNomeAllegato() {
		return nomeAllegato;
	}
	public void setNomeAllegato(List<String> nomeAllegato) {
		this.nomeAllegato = nomeAllegato;
	}
	public List<String> getFormatoAllegato() {
		return formatoAllegato;
	}
	public void setFormatoAllegato(List<String> formatoAllegato) {
		this.formatoAllegato = formatoAllegato;
	}
	public String getStatoSdi() {
		return statoSdi;
	}
	public void setStatoSdi(String statoSdi) {
		this.statoSdi = statoSdi;
	}
	public List<String> getCompressioneAllegato() {
		return compressioneAllegato;
	}
	public void setCompressioneAllegato(List<String> compressioneAllegato) {
		this.compressioneAllegato = compressioneAllegato;
	}
	public String getRegistroIva() {
		return registroIva;
	}
	public void setRegistroIva(String registroIva) {
		this.registroIva = registroIva;
	}
	public String getCodice() {
		return codice;
	}
	public void setCodice(String codice) {
		this.codice = codice;
	}
	public String getCodiceFiscale() {
		return codiceFiscale;
	}
	public void setCodiceFiscale(String codiceFiscale) {
		this.codiceFiscale = codiceFiscale;
	}
	public String getRagioneSocialeGestionale() {
		return ragioneSocialeGestionale;
	}
	public void setRagioneSocialeGestionale(String ragioneSocialeGestionale) {
		this.ragioneSocialeGestionale = ragioneSocialeGestionale;
	}
	public String getDataRegistrazione() {
		return dataRegistrazione;
	}
	public void setDataRegistrazione(String dataRegistrazione) {
		this.dataRegistrazione = dataRegistrazione;
	}
	public String getRegFiscale() {
		return regFiscale;
	}
	public void setRegFiscale(String regFiscale) {
		this.regFiscale = regFiscale;
	}
	public String getCausale() {
		return causale;
	}
	public void setCausale(String causale) {
		this.causale = causale;
	}
	public String getCodiceFiscaleCliente() {
		return codiceFiscaleCliente;
	}
	public void setCodiceFiscaleCliente(String codiceFiscaleCliente) {
		this.codiceFiscaleCliente = codiceFiscaleCliente;
	}
	public String getPartitaIvaCliente() {
		return partitaIvaCliente;
	}
	public void setPartitaIvaCliente(String partitaIvaCliente) {
		this.partitaIvaCliente = partitaIvaCliente;
	}
	public String getBolloVirtuale() {
		return bolloVirtuale;
	}
	public void setBolloVirtuale(String bolloVirtuale) {
		this.bolloVirtuale = bolloVirtuale;
	}
	public Double getImportoBollo() {
		return importoBollo;
	}
	public void setImportoBollo(Double importoBollo) {
		this.importoBollo = importoBollo;
	}
	
}
