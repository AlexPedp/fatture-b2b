package com.progettoedp.data;

public class Ordine {
	
	String name;
	String partitaIva;
	String note;
	String cityName;
	String idOrder;
	String idDelivery;
	String issueDate;
	String nomeXml;
	String dettagli;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public String getIdOrder() {
		return idOrder;
	}
	public void setIdOrder(String idOrder) {
		this.idOrder = idOrder;
	}
	public String getIdDelivery() {
		return idDelivery;
	}
	public void setIdDelivery(String idDelivery) {
		this.idDelivery = idDelivery;
	}
	public String getIssueDate() {
		return issueDate;
	}
	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}
	public String getNomeXml() {
		return nomeXml;
	}
	public void setNomeXml(String nomeXml) {
		this.nomeXml = nomeXml;
	}
	public String getDettagli() {
		return dettagli;
	}
	public void setDettagli(String dettagli) {
		this.dettagli = dettagli;
	}
	public String getPartitaIva() {
		return partitaIva;
	}
	public void setPartitaIva(String partitaIva) {
		this.partitaIva = partitaIva;
	}
	
}
