package com.progettoedp.data;

public class Customer {
	
	String partitaIva;
	String separatore;
	int occorrenza;
	String allineamento;
	
	public String getPartitaIva() {
		return partitaIva;
	}
	
	public void setPartitaIva(String partitaIva) {
		this.partitaIva = partitaIva;
	}
	
	public String getSeparatore() {
		return separatore;
	}
	
	public void setSeparatore(String separatore) {
		this.separatore = separatore;
	}
	
	public String getAllineamento() {
		return allineamento;
	}
	
	public void setAllineamento(String allineamento) {
		this.allineamento = allineamento;
	}

	public int getOccorrenza() {
		return occorrenza;
	}

	public void setOccorrenza(int occorrenza) {
		this.occorrenza = occorrenza;
	}
	
}
