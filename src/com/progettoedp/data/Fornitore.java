package com.progettoedp.data;

public class Fornitore {
	
	private String codice;
	private String ragioneSociale;
	private String classe;
	private String descrizione;
	private String partitaIva;
	private int sottoconto;
	
	public String getCodice() {
		return codice;
	}
	public void setCodice(String codice) {
		this.codice = codice;
	}
	public String getRagioneSociale() {
		return ragioneSociale;
	}
	public void setRagioneSociale(String ragioneSociale) {
		this.ragioneSociale = ragioneSociale;
	}
	public String getClasse() {
		return classe;
	}
	public void setClasse(String classe) {
		this.classe = classe;
	}
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
	public String getPartitaIva() {
		return partitaIva;
	}
	public void setPartitaIva(String partitaIva) {
		this.partitaIva = partitaIva;
	}
	public int getSottoconto() {
		return sottoconto;
	}
	public void setSottoconto(int sottoconto) {
		this.sottoconto = sottoconto;
	}
 
	
}
