package com.progettoedp.parser;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.codec.binary.Hex;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.progettoedp.data.Dettaglio;
import com.progettoedp.data.Fattura;
import com.progettoedp.data.Pagamento;
import com.progettoedp.data.Riepilogo;
import com.progettoedp.data.Tag;
import com.progettoedp.data.TagType;
import com.progettoedp.main.Application;
import com.progettoedp.testo.Testo;
import com.progettoedp.util.Logger;



public class XMLParser {

	private static final Logger logger = Logger.getLogger(XMLParser.class);
	private static final String TAG = "XMLParser";

	private File file;
	private DocumentBuilderFactory dbFactory;
	private DocumentBuilder dBuilder;
	private Document doc;
	private LinkedHashMap <String, Tag> nodes = new LinkedHashMap<String, Tag>();
	private Fattura fattura; 
	private Dettaglio dettaglio;
	private Riepilogo riepilogo;
	private Pagamento pagamento;
	private List<String> allegati;
	private List<String> nomeAllegati;
	private List<String> formatoAllegati;
	private List<String> compressioneAllegati;
	private List<Dettaglio> dettagli;
	private List<Riepilogo> riepiloghi;
	private List<Pagamento> pagamenti;
	private String partitaIva;

	public XMLParser(File file){
		logger.debug(TAG, "XML file parsing: " + file.getAbsolutePath());
		this.file = file;
		init();
	}

	public void init(){
		partitaIva = Application.getInstance().getPartitaIva();

		fattura = new Fattura();
		allegati = new ArrayList<String>();
		nomeAllegati = new ArrayList<String>();
		formatoAllegati = new ArrayList<String>();
		compressioneAllegati = new ArrayList<String>();
		dettagli = new ArrayList<Dettaglio>();
		riepiloghi = new ArrayList<Riepilogo>();
		pagamenti = new ArrayList<Pagamento>();

		{
			Tag elementNode =  new Tag();
			elementNode.add(Testo.idCodice);
			TagType tagType = new TagType();
			tagType.setRepeated(false);
			tagType.setType(Testo.datiTrasmissione);
			elementNode.setTagType(tagType);
			nodes.put(Testo.datiTrasmissione, elementNode);
		}

		{
			Tag elementNode =  new Tag();
			elementNode.add(Testo.numero);
			elementNode.add(Testo.data);
			elementNode.add(Testo.tipoDocumento);
			elementNode.add(Testo.importoTotaleDocumento);
			elementNode.add(Testo.causale);
			elementNode.add(Testo.bolloVirtuale);
			elementNode.add(Testo.importoBollo);
			TagType tagType = new TagType();
			tagType.setRepeated(false);
			tagType.setType(Testo.datiGeneraliDocumento);
			elementNode.setTagType(tagType);
			nodes.put(Testo.datiGeneraliDocumento, elementNode);
		}


		{
			Tag elementNode =  new Tag();
			elementNode.add(Testo.idCodice);
			elementNode.add(Testo.codiceFiscale);
			elementNode.add(Testo.denominazione);
			elementNode.add(Testo.nome);
			elementNode.add(Testo.cognome);
			TagType tagType = new TagType();
			tagType.setRepeated(false);
			tagType.setType(Testo.cedentePrestatore);
			elementNode.setTagType(tagType);
			nodes.put(Testo.cedentePrestatore, elementNode);
		}

		{
			Tag elementNode =  new Tag();
			elementNode.add(Testo.idCodice);
			elementNode.add(Testo.idPaese);

			TagType tagType = new TagType();
			tagType.setRepeated(false);
			tagType.setType(Testo.cessionarioCommittente);
			elementNode.setTagType(tagType);
			nodes.put(Testo.cessionarioCommittente, elementNode);
		}

		{
			Tag elementNode =  new Tag();
			elementNode.add(Testo.attachment);
			elementNode.add(Testo.formatoAttachment);
			elementNode.add(Testo.nomeAttachment);
			elementNode.add(Testo.compressioneAttachment);
			TagType tagType = new TagType();
			tagType.setRepeated(false);
			tagType.setType(Testo.attachment);
			elementNode.setTagType(tagType);
			nodes.put(Testo.allegati, elementNode);
		}

		{
			Tag elementNode =  new Tag();
			elementNode.add(Testo.descrizione);
			elementNode.add(Testo.codiceValore);
			elementNode.add(Testo.quantita);
			elementNode.add(Testo.prezzoTotale);
			elementNode.add(Testo.prezzoUnitario);
			elementNode.add(Testo.aliquotaIVA);
			elementNode.add(Testo.riga);
			TagType tagType = new TagType();
			tagType.setRepeated(true);
			tagType.setType(Testo.dettaglioLinee);
			elementNode.setTagType(tagType);
			nodes.put(Testo.dettaglioLinee, elementNode);

		}

		{
			Tag elementNode =  new Tag();
			elementNode.add(Testo.imponibileImporto);
			elementNode.add(Testo.imposta);
			elementNode.add(Testo.aliquotaIVA);
			elementNode.add(Testo.natura);
			TagType tagType = new TagType();
			tagType.setRepeated(true);
			tagType.setType(Testo.datiRiepilogo);
			elementNode.setTagType(tagType);
			nodes.put(Testo.datiRiepilogo, elementNode);
		}

		{
			Tag elementNode =  new Tag();
			elementNode.add(Testo.condizioniPagamento);
			elementNode.add(Testo.modalitaPagamento);
			elementNode.add(Testo.dataScadenzaPagamento);
			elementNode.add(Testo.importoPagamento);
			TagType tagType = new TagType();
			tagType.setRepeated(true);
			tagType.setType(Testo.datiPagamento);
			elementNode.setTagType(tagType);
			nodes.put(Testo.datiPagamento, elementNode);
		}

	}

	public Fattura run(){

		try {

			InputStream inputStream= new FileInputStream(file);
			Reader reader = new InputStreamReader(inputStream,"UTF-8");
			InputSource is = new InputSource(reader);
			is.setEncoding("UTF-8");

			removeBom(file);

			dbFactory = DocumentBuilderFactory.newInstance();
			dBuilder = dbFactory.newDocumentBuilder();
			doc = dBuilder.parse(is);

			doc.getDocumentElement().normalize();


			for(String nodeName : nodes.keySet()){
				Tag tag = nodes.get(nodeName);
				if(tag!=null){
					readNode(nodeName, tag);
				}
			}

		} catch (Exception e) {
			logger.error(TAG, "Exception: " + " Nome File " + file.getAbsolutePath() + e); 
			return null;
		}

		logger.debug(TAG, "Parsed Fattura: " + fattura.getPartitaIva() +  "|" + fattura.getTipoDocumento() + "|" + fattura.getData() + "|" + fattura.getNumero() + "|" + file.getName());
		fattura.setDettagli(dettagli);
		fattura.setRiepiloghi(riepiloghi);
		fattura.setPagamenti(pagamenti);

		try {
			for (Riepilogo r: riepiloghi){
				if (r.getNatura()!=null && r.getImponibile().equals(Testo.IMPORTO_BOLLO)) {
					if(r.getNatura().trim().equals(Testo.NATURA_BOLLO)|| r.getNatura().trim().equals(Testo.NATURA_BOLLO_AGENTE)){
						fattura.setBolloVirtuale("NO");
						fattura.setImportoBollo(r.getImponibile());
					}
				}
			}
		} catch (Exception e) {
			logger.error(TAG, "Erorre in bollo " + e);
		}

		return fattura;

	}

	public void readNode(String nodeName, Tag elementName){

		NodeList nList = doc.getElementsByTagName(nodeName);

		for (int temp = 0; temp < nList.getLength(); temp++) {

			TagType tagType = elementName.getTagType();

			if (tagType.isRepeated()){

				switch (tagType.getType()) {
				case Testo.dettaglioLinee:
					dettaglio = new Dettaglio();
					break;
				case Testo.datiRiepilogo:
					riepilogo = new Riepilogo();
					break;
				case Testo.datiPagamento:
					pagamento = new Pagamento();
					break;
				default:
					break;
				}


			}

			Node nNode = nList.item(temp);

			logger.debug(TAG, "TAG Lettura node:"  + nodeName);

			if (nNode.getNodeType() == Node.ELEMENT_NODE) {

				Element element = (Element) nNode;

				List<String> elements = elementName.getElements();

				for (String name : elements){

					try {

						String value =  element.getElementsByTagName(name).item(0).getTextContent();
						logger.debug(TAG, "Node: " + nodeName + " Element: " + name + ":" + value);
						updaateTag(element, name, tagType);

					} catch (Exception e) {
						logger.debug(TAG, "Errore:" + " Elemento non trovato: " + name);
					}


				}

			} 

			if (tagType.isRepeated()){

				switch (tagType.getType()) {
				case Testo.dettaglioLinee:
					dettagli.add(dettaglio);
					break;
				case Testo.datiRiepilogo:
					riepiloghi.add(riepilogo);
					break;
				case Testo.datiPagamento:
					pagamenti.add(pagamento);
					break;
				default:
					break;
				}


			}


		}

	}


	public void updaateTag(Element element, String tag, TagType tagtype){

		Node node = element.getElementsByTagName(tag).item(0);
		String value =  element.getElementsByTagName(tag).item(0).getTextContent();

		switch (tag) {
		case Testo.idCodice:
			switch (tagtype.getType()) {
			case Testo.datiTrasmissione:
				if(partitaIva.equals(value)){
					fattura.setTipo(Testo.ATTIVA);	
				} else {
					fattura.setTipo(Testo.PASSIVA);
				}
				fattura.setPartitaIvaMittente(value);
				break;
			case Testo.cedentePrestatore:
				fattura.setPartitaIva(value);
				break;
			case Testo.cessionarioCommittente:
				fattura.setPartitaIvaCliente(value);
				break;
			}
			break;
		case Testo.idPaese:
			fattura.setNazione(value);
			break;
		case Testo.attachment:
			allegati.add(value);
			fattura.setAllegato(allegati);
			break;
		case Testo.formatoAttachment:
			formatoAllegati.add(value);
			fattura.setFormatoAllegato(formatoAllegati);
			break;
		case Testo.compressioneAttachment:
			compressioneAllegati.add(value);
			fattura.setCompressioneAllegato(compressioneAllegati);
			break;
		case Testo.nomeAttachment:
			nomeAllegati.add(value);
			fattura.setNomeAllegato(nomeAllegati);
			break;
		case Testo.codiceFiscale:
			switch (tagtype.getType()) {
			case Testo.datiTrasmissione:
				break;
			case Testo.cedentePrestatore:
				fattura.setCodiceFiscale(value);
				break;
			case Testo.cessionarioCommittente:
				fattura.setCodiceFiscaleCliente(value);
				break;
			}
			break;
		case Testo.denominazione:
			fattura.setRagioneSociale(value);
			break;
		case Testo.nome:
			fattura.setNome(value);
			break;
		case Testo.cognome:
			fattura.setCognome(value);
			break;
		case Testo.data:
			fattura.setData(value);
			break; 
		case Testo.numero:
			fattura.setNumero(value);
			break;
		case Testo.tipoDocumento:
			fattura.setTipoDocumento(value);
			break;
		case Testo.importoTotaleDocumento:
			fattura.setTotaleFattura(Double.parseDouble(value));
			break;
		case Testo.causale:
			fattura.setCausale(value);
			break;
		case Testo.bolloVirtuale:
			fattura.setBolloVirtuale(value);
			break;
		case Testo.importoBollo:
			//			fattura.setImportoBollo(Double.parseDouble(value));
			break;
		case Testo.descrizione:
			dettaglio.setDescrizione(value);
			break;	
		case Testo.codiceValore:
			dettaglio.setArticolo(value);
			break;	
		case Testo.riga:
			dettaglio.setRiga(Integer.parseInt(value));
			break;	
		case Testo.quantita:
			dettaglio.setQuantita(Double.parseDouble(value));
			break;	
		case Testo.aliquotaIVA:
			switch (tagtype.getType()) {
			case Testo.datiRiepilogo:
				riepilogo.setIva(value);
				break;
			default:
				dettaglio.setIva(value);
				break;
			}
			break;	
		case Testo.natura:
			switch (tagtype.getType()) {
			case Testo.datiRiepilogo:
				riepilogo.setNatura(value);
				break;
			default:
				dettaglio.setNatura(value);
				break;
			}
			break;	
		case Testo.prezzoUnitario:
			dettaglio.setPrezzoUnitario(Double.parseDouble(value));
			break;	
		case Testo.prezzoTotale:
			dettaglio.setPrezzoTotale(Double.parseDouble(value));
			break;	
		case Testo.modalitaPagamento:
			pagamento.setModalita(value);
			break;
		case Testo.importoPagamento:
			pagamento.setImporto(Double.parseDouble(value));
			break;	
		case Testo.dataScadenzaPagamento:
			pagamento.setScadenza(value);
			break;	
			//		case Testo.aliquotaIVA:
			//		
		case Testo.imponibileImporto:
			riepilogo.setImponibile(Double.parseDouble(value));
			break;	
		case Testo.imposta:
			riepilogo.setImposta(Double.parseDouble(value));
			break;	
		case Testo.esigibilita:
			riepilogo.setEsigibilitaIva(value);
			break;
		default:
			break;
		}

	}

	public boolean isContainBOM(File file) throws IOException {

		//	      if (Files.notExists(path)) {
		//	    	  logger.error(TAG, "File not exist");
		//	    	  return false;
		//	      }

		boolean result = false;

		byte[] bom = new byte[3];
		try (InputStream is = new FileInputStream(file)) {

			is.read(bom);

			String content = new String(Hex.encodeHex(bom));
			if ("efbbbf".equalsIgnoreCase(content)) {
				result = true;
			}

		}

		return result;
	}

	public void removeBom(File file) throws IOException {

		if (isContainBOM(file)) {

			byte[] bytes = Files.readAllBytes(file.toPath());

			ByteBuffer bb = ByteBuffer.wrap(bytes);

			logger.info(TAG, "Found BOM in file: " + file.getAbsolutePath());

			byte[] bom = new byte[3];

			bb.get(bom, 0, bom.length);


			byte[] contentAfterFirst3Bytes = new byte[bytes.length - 3];
			bb.get(contentAfterFirst3Bytes, 0, contentAfterFirst3Bytes.length);

			logger.debug(TAG, "Remove the first 3 bytes, and overwrite the file");


			Files.write(file.toPath(), contentAfterFirst3Bytes);

		} else {
			logger.debug(TAG, "This file doesn't contains UTF-8 BOM");
		}

	}

}
