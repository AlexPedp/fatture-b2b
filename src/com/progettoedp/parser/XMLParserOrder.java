package com.progettoedp.parser;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.LinkedHashMap;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.progettoedp.data.Ordine;
import com.progettoedp.data.Tag;
import com.progettoedp.data.TagType;
import com.progettoedp.testo.Testo;
import com.progettoedp.util.Logger;
 


public class XMLParserOrder {

	private static final Logger logger = Logger.getLogger(XMLParserOrder.class);
	private static final String TAG = "XMLParser";
	private Ordine order; 
	private File file;
	private DocumentBuilderFactory dbFactory;
	private DocumentBuilder dBuilder;
	private Document doc;
	private LinkedHashMap <String, Tag> nodes = new LinkedHashMap<String, Tag>();
	
	public XMLParserOrder(File file){
		this.file = file;
		init();
	}

	public void init(){
		
		order = new Ordine();
		
		{
			Tag elementNode =  new Tag();
			elementNode.add(Testo.ID);
			elementNode.add(Testo.issueDate);
			elementNode.add(Testo.note);
			TagType tagType = new TagType();
			tagType.setRepeated(false);
			tagType.setType(Testo.datiOrder);
			elementNode.setTagType(tagType);
			nodes.put(Testo.datiOrder, elementNode);
		}
	 
		{
			Tag elementNode =  new Tag();
			elementNode.add(Testo.name);
			elementNode.add(Testo.companyID);
			TagType tagType = new TagType();
			tagType.setRepeated(false);
			tagType.setType(Testo.buyerCustomerParty);
			elementNode.setTagType(tagType);
			nodes.put(Testo.buyerCustomerParty, elementNode);
		}

		{
			Tag elementNode =  new Tag();
			elementNode.add(Testo.ID);
			elementNode.add(Testo.name);
			TagType tagType = new TagType();
			tagType.setRepeated(false);
			tagType.setType(Testo.deliveryLocation);
			elementNode.setTagType(tagType);
			nodes.put(Testo.deliveryLocation, elementNode);
		}
 

	}

	public Ordine run(){
		 
		try {
			
			InputStream inputStream= new FileInputStream(file);
			Reader reader = new InputStreamReader(inputStream,"UTF-8");
			InputSource is = new InputSource(reader);
			is.setEncoding("UTF-8");
 
			dbFactory = DocumentBuilderFactory.newInstance();
			dBuilder = dbFactory.newDocumentBuilder();
 			doc = dBuilder.parse(is);

			doc.getDocumentElement().normalize();

			 
			for(String nodeName : nodes.keySet()){
				Tag tag = nodes.get(nodeName);
				if(tag!=null){
					readNode(nodeName, tag);
				}
			}

		} catch (Exception e) {
			logger.error(TAG, "Exception: " + e); 
		}
		
		logger.debug(TAG, "Parsed Order: " + order.getIdOrder() +  "|" + order.getNote() + "|" + order.getCityName() + "|" + order.getIdDelivery() + "|" + file.getName());
		return order;
		
	}

	public void readNode(String nodeName, Tag elementName){

		NodeList nList = doc.getElementsByTagName(nodeName);

		for (int temp = 0; temp < nList.getLength(); temp++) {
			
			TagType tagType = elementName.getTagType();
			
			if (tagType.isRepeated()){
				
				 
				
			}

			Node nNode = nList.item(temp);

			logger.debug(TAG, "TAG Lettura node:"  + nodeName);

			if (nNode.getNodeType() == Node.ELEMENT_NODE) {

				Element element = (Element) nNode;

				List<String> elements = elementName.getElements();

				for (String name : elements){

					try {

						String value =  element.getElementsByTagName(name).item(0).getTextContent();
						logger.debug(TAG, "Node: " + nodeName + " Element: " + name + ":" + value);
						updaateTag(element, name, tagType);
						 
					} catch (Exception e) {
						logger.debug(TAG, "Errore:" + " Elemento non trovato: " + name);
					}
					

				}

			} 
			
			if (tagType.isRepeated()){
				
				 
				
				
			}


		}

	}

 
	public void updaateTag(Element element, String tag, TagType tagtype){

		Node node = element.getElementsByTagName(tag).item(0);
		String value =  element.getElementsByTagName(tag).item(0).getTextContent();

		switch (tag) {
		case Testo.ID:
			switch (tagtype.getType()) {
			case Testo.datiOrder:
				order.setIdOrder(value);
				break;
			case Testo.deliveryLocation:
				order.setIdDelivery(value);
				break;
			}
			break;
		case Testo.note:
			order.setNote(value);
			break;
		case Testo.companyID:
			order.setPartitaIva(value);
			break;
		case Testo.name:
			switch (tagtype.getType()) {
			case Testo.buyerCustomerParty:
				order.setName(value);
				break;
			case Testo.deliveryLocation:
				order.setCityName(value);
				break;
			}
			break;
		case Testo.issueDate:
			order.setIssueDate(value);
			break;
		default:
			break;
		}

	}

}
