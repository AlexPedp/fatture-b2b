package com.progettoedp.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import com.progettoedp.data.Euroquote;
import com.progettoedp.data.Fattura;
import com.progettoedp.data.Fornitore;
import com.progettoedp.data.Ordine;
import com.progettoedp.data.Registrazione;
import com.progettoedp.data.Sdi;
import com.progettoedp.testo.Testo;
import com.progettoedp.util.Logger;


public class DAO {

	private static final Logger logger = Logger.getLogger(DAO.class);

	private static final String TAG = "DAO";


	public static String type;

	static final String TABLE_FATTURE = "FATTURE";

	private DatabaseManager sqliteManager;
	private DatabaseManager mssqlManager;
	private DatabaseManager mssqlManagerEq;
	private DatabaseManager mssqlManagerApp;
	private DatabaseManager as400Manager;

	public DAO(){

	}

	public void setSqliteManager(DatabaseManager dm){
		this.sqliteManager = dm;
	}

	public void setMssqlManager(DatabaseManager dm){
		this.mssqlManager = dm;
	}

	public void setMssqlManagerApp(DatabaseManager dm){
		this.mssqlManagerApp = dm;
	}

	public void setAs400Manager(DatabaseManager dm){
		this.as400Manager = dm;
	}

	public void setMssqlManagerEq(DatabaseManager dm){
		this.mssqlManagerEq = dm;
	}
	 
	public void setType(String type){
		this.type = type;
	}

	public void init(String databaseType) {

		Connection conn = null;

		try {



			switch (databaseType) {
			case Testo.SQLITE:


				conn = sqliteManager.getConnection();
				// creiamo le tabelle

				try {
					Statement stat;
					stat = conn.createStatement();
					// stat.executeUpdate("drop table if exists people;");
					stat.executeUpdate("CREATE TABLE " + TABLE_FATTURE
							+ " (" + "ID INTEGER  PRIMARY KEY AUTOINCREMENT NOT NULL, "
							+ "ATT_PASS TEXT," + "TIPO TEXT, " + "NUMERO TEXT, " + "DATA TEXT, "
							+ "PARTITA_IVA TEXT," + "RAGIONE_SOCIALE TEXT, " + "TOTALE REAL, " + "ALLEGATO TEXT,"
							+ "DETTAGLI TEXT, " + "RIEPILOGHI TEXT, " + "PAGAMENTI TEXT,"   
							+ "DATA_RITIRO TEXT, " + "DATA_SDI_1 TEXT, " + "DATA_SDI_2 TEXT, " + "ID_SDI TEXT, " + "STATO_SDI TEXT, " 
							+ "NOME_XML TEXT," + "NUMERO_PRO_EQ TEXT," + "DATA_PRO_EQ TEXT," 
							+ "DATA_RICEZIONE TEXT," + "DATA_CONSEGNA TEXT," 
							+ "STATO TEXT," + "REG_FISCALE TEXT, " + "CAUSALE TEXT, "
							+ "PARTITA_IVA_CLI TEXT, CODICE_FISCALE_CLI TEXT, "
							+ "BOLLO_VIRTUALE TEXT, IMPORTO_BOLLO REAL"
							+ ")");


				} catch (SQLException e) {

				}



				break;

			case Testo.MSSQL:

				conn = mssqlManagerApp.getConnection();
				// creiamo le tabelle

				try {
					Statement stat;
					stat = conn.createStatement();
					// stat.executeUpdate("drop table if exists people;");
					stat.executeUpdate("CREATE TABLE FATTUREB2B.DBO.FATTURE ("
							+	"ID INT NOT NULL IDENTITY PRIMARY KEY,"
							+	"ATT_PASS VARCHAR(10),"
							+	"TIPO VARCHAR(10),"
							+	"NUMERO VARCHAR(50),"
							+	"DATA VARCHAR(20),"
							+	"PARTITA_IVA VARCHAR(50),"
							+	"RAGIONE_SOCIALE VARCHAR(100),"
							+	"TOTALE DECIMAL(20, 6),"
							+	"ALLEGATO VARCHAR(MAX),"
							+	"DETTAGLI VARCHAR(MAX),"
							+	"RIEPILOGHI VARCHAR(MAX),"
							+	"PAGAMENTI VARCHAR(MAX),"
							+	"DATA_RITIRO VARCHAR(30),"
							+	"DATA_SDI_1 VARCHAR(30),"
							+	"DATA_SDI_2 VARCHAR(30),"
							+	"ID_SDI VARCHAR(50),"
							+	"STATO_SDI VARCHAR(50),"
							+	"NOME_XML VARCHAR(50),"
							+	"NUMERO_PRO_EQ VARCHAR(50),"
							+	"DATA_PRO_EQ VARCHAR(30),"
							+	"DATA_RICEZIONE VARCHAR(30),"
							+	"DATA_CONSEGNA VARCHAR(30),"
							+	"STATO VARCHAR(50),"
							+	"REG_FISCALE VARCHAR(10), "
							+	"CAUSALE VARCHAR(50), "
							+   "PARTITA_IVA_CLI VARCHAR(50),"
							+   "CODICE_FISCALE_CLI VARCHAR(50), "
							+   "BOLLO_VIRTUALE TEXT VARCHAR(10), " 
							+   "IMPORTO_BOLLO DECIMAL(20, 6)"
							+ ")");



				} catch (SQLException e) {

				}


				break;

			default:
				break;
			}

			try {
				DAOFatture.creaIndici(conn);
			} catch (SQLException e) {
				// mLogger.e(TAG, e.getMessage());
				// ignore,
				// table already exists...
			}


		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
			}
		}



	}



	public boolean insertFattura(Fattura fattura) {
		Connection connection = null;
		boolean insert = false;
		try {
			connection = getConnection(type);
//			connection = sqliteManager.getConnection();
			insert = DAOFatture.insert(connection, fattura);
		} finally {
			silentClose(connection);
		}
		return insert;
	}
	
	public boolean insertOrdine(Ordine ordine) {
		Connection connection = null;
		boolean insert = false;
		try {
			connection = getConnection(type);
//			connection = sqliteManager.getConnection();
			insert = DAOFatture.insertOrdine(connection, ordine);
		} finally {
			silentClose(connection);
		}
		return insert;
	}
	
	public boolean insertFatturaFromMigrate(Fattura fattura, String type) {
		Connection connection = null;
		boolean insert = false;
		try {
			connection = getConnection(type);
			insert = DAOFatture.insert(connection, fattura);
		} finally {
			silentClose(connection);
		}
		return insert;
	}

	public boolean updateFattura(Fattura fattura, String statoSdi) {
		Connection connection = null;
		boolean update = false;	
		try {
//			connection = sqliteManager.getConnection();
			connection = getConnection(type);
			update = DAOFatture.updateFattura(connection, fattura, statoSdi);
		} finally {
			silentClose(connection);
		}
		return update;
	}

	public Fattura findFatturaByNumero(String tipo, String tipoDoc, String data, String numero, String piva) {
		Connection connection = null;
		Fattura fattura;
		try {
//			connection = sqliteManager.getConnection();
			connection = getConnection(type);
			fattura = DAOFatture.findFatturaByNumero(connection, tipo, tipoDoc, data, numero, piva);
		} finally {
			silentClose(connection);
		}
		return fattura;
	}
	
	public Ordine findOrdineByNumero(String tipo, String tipoDoc, String data, String numero, String piva) {
		Connection connection = null;
		Ordine ordine;
		try {
//			connection = sqliteManager.getConnection();
			connection = getConnection(type);
			ordine = DAOFatture.findOrdineByNumero(connection, tipo, tipoDoc, data, numero, piva);
		} finally {
			silentClose(connection);
		}
		return ordine;
	}



	public boolean deleteFatturaByNumero(String tipo, String tipoDoc, String data, String numero, String piva) {
		Connection connection = null;
		boolean delete = false;
		try {
//			connection = sqliteManager.getConnection();
			connection = getConnection(type);
			delete = DAOFatture.deleteFatturaByNumero(connection, tipo, tipoDoc, data, numero, piva);
		} finally {
			silentClose(connection);
		}
		return delete;
	}

	public List<Fattura> findfattureByTipoAndStato(String tipo, String stato, boolean spaccio, String parametro) {
		Connection connection = null;
		List<Fattura> fatture;
		try {
//			connection = sqliteManager.getConnection();
			connection = getConnection(type);
			fatture = DAOFatture.findfattureByTipoAndStato(connection, tipo, stato, spaccio, parametro);
		} finally {
			silentClose(connection);
		}
		return fatture;
	}
	
	public List<Fattura> findAllFatture(int limit, int offset) {
		Connection connection = null;
		List<Fattura> fatture;
		try {
//			connection = sqliteManager.getConnection();
			connection = getConnection(type);
			fatture = DAOFatture.findAllFatture(connection, limit, offset);
		} finally {
			silentClose(connection);
		}
		return fatture;
	}

	public boolean updateFattureByTipoAndStato(String tipo, String stato) {
		Connection connection = null;
		boolean delete = false;
		try {
//			connection = sqliteManager.getConnection();
			connection = getConnection(type);
			delete = DAOFatture.updateFattureByTipoAndStato(connection, tipo, stato);
		} finally {
			silentClose(connection);
		}
		return delete;
	}


	public List<Sdi> readDataSdi() throws SQLException {
		Connection connection = null;
		List<Sdi> sdi;
		try {
			connection = mssqlManager.getConnection();
			sdi = DAOSdi.readDataSdi(connection);
		} finally {
			silentClose(connection);
		}
		return sdi;
	}
	
	public Sdi readDataSdiByNumero(String tipoDoc, String data, String numero, String piva) throws SQLException {
		Connection connection = null;
		Sdi sdi;
		try {
			connection = mssqlManager.getConnection();
			sdi = DAOSdi.readDataSdiByNumero(connection, tipoDoc, data, numero, piva);
		} finally {
			silentClose(connection);
		}
		return sdi;
	}

	public List<Sdi> readDataSdiActive() throws SQLException {
		Connection connection = null;
		List<Sdi> sdi;
		try {
			connection = mssqlManager.getConnection();
			sdi = DAOSdi.readDataSdiActive(connection);
		} finally {
			silentClose(connection);
		}
		return sdi;
	}

	public Euroquote readEuroquote(String numero, String data, String partitaIva) throws SQLException {
		Connection connection = null;
		Euroquote euroquote;
		try {
			connection = mssqlManagerEq.getConnection();
			euroquote = DAOEuroquote.readProtocollo(connection, numero, data, partitaIva);
		} finally {
			silentClose(connection);
		}
		return euroquote;
	}
	
	public boolean updateEuroquote(String numero, String data, String partitaIva, String numPro, String dataPro) throws SQLException {
		Connection connection = null;
		boolean result;
		try {
			connection = mssqlManagerEq.getConnection();
			result = DAOEuroquote.updateProtocollo(connection, numero, data, partitaIva, numPro, dataPro);
		} finally {
			silentClose(connection);
		}
		return result;
	}

	public boolean insertDataAs400(Fattura fattura, String uuid, int row) throws SQLException {
		Connection connection = null;
		boolean insert = false;
		try {
			connection = as400Manager.getConnection();
			insert = DAOAS400.insertDati(connection, uuid, row, fattura);
		} finally {
			silentClose(connection);
		}
		return insert;
	}
	
	public boolean insertOrdineAs400(Ordine ordine, String uuid, int row) throws SQLException {
		Connection connection = null;
		boolean insert = false;
		try {
			connection = as400Manager.getConnection();
			insert = DAOAS400.insertOrdine(connection, uuid, row, ordine);
		} finally {
			silentClose(connection);
		}
		return insert;
	}

	public Fornitore readFornitore(String partitaIva, String codiceFiscale) throws SQLException {
		Connection connection = null;
		Fornitore fornitore = null;
		try {
			connection = as400Manager.getConnection();
			fornitore = DAOAS400.readFornitore(connection, partitaIva, codiceFiscale);
		}catch (Exception e) {
			logger.error(TAG, e);
		} 

		finally {
			silentClose(connection);
		}
		return fornitore;
	}
	
	public Registrazione readProtocollo(String idSdi) throws SQLException {
		Connection connection = null;
		Registrazione registrazione = null;
		try {
			connection = as400Manager.getConnection();
			registrazione = DAOAS400.readProtocollo(connection, idSdi);
		}catch (Exception e) {
			logger.error(TAG, e);
		} 

		finally {
			silentClose(connection);
		}
		return registrazione;
	}

	public boolean insertCmdAs400(String uuid, String ccie) throws SQLException {
		Connection connection = null;
		boolean insert = false;
		try {
			connection = as400Manager.getConnection();
			insert = DAOAS400.insertCmd(connection, uuid, ccie);
		} finally {
			silentClose(connection);
		}
		return insert;
	}


	public static DAO create(DatabaseManager sqliteManager, DatabaseManager mssqlManager, DatabaseManager as400Manager, DatabaseManager mssqlManagerEq, DatabaseManager mssqlManagerApp){
		DAO dao = new DAO();
		dao.setSqliteManager(sqliteManager);
		dao.setMssqlManager(mssqlManager);
		dao.setAs400Manager(as400Manager);
		dao.setMssqlManagerEq(mssqlManagerEq);
		dao.setMssqlManagerApp(mssqlManagerApp);
		return dao;
	}

	public static void silentClose(Connection conn) {
		try {
			if (conn != null)
				conn.close();
		} catch (Exception e) {
			// ignore
		}
	}
	
	public Connection getConnection(String type){
		Connection conn = null;
		switch (type) {
		case Testo.SQLITE:
			conn = sqliteManager.getConnection();
			break;
		case Testo.MSSQL:
			conn = mssqlManagerApp.getConnection();
			break;

		default:
			break;
		}
		return conn;
	}


}
