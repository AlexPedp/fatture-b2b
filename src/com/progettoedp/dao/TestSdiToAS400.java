package com.progettoedp.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.progettoedp.data.Fattura;
import com.progettoedp.data.Pagamento;
import com.progettoedp.data.Sdi;
import com.progettoedp.main.Application;
import com.progettoedp.testo.Testo;

public class TestSdiToAS400 {

	private static final String host ="192.168.50.6";
	private static final String port = "1433";
	private static final String user = "sa";
	private static final String password = "milkuht";
	private static final String databaseName = "SOL1GFAT_MAST";

	public static void main(String[] args) {
		System.out.println("-------- MySQL JDBC Connection Testing ------------");

		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		} catch (ClassNotFoundException e) {

			System.out.println("Where is your MSSQL JDBC Driver?");
			e.printStackTrace();
			return;
		}

		System.out.println("MSSQL JDBC Driver Registered!");
		Connection connection = null;


		try {

			String url = "jdbc:sqlserver://" + host + ";" + "DatabaseName=" + databaseName;
			System.out.println(url);
			connection = DriverManager.getConnection(url,user, password);

			List<Sdi> readDataSdiActive = DAOSdi.readDataSdiActive(connection);
			List<Fattura> fattureAttive = new ArrayList<Fattura>();

			for(Sdi s: readDataSdiActive){
				String tipo = s.getTipo();
				String numero = s.getNumeroDoc();
				String data = s.getDataDoc().substring(0, 10);
				String dataRicezione = s.getDataRicezione();
				String dataConsegna = s.getDataConsegna();
				String protocollo = s.getIdSdi();
				String nomeXml = s.getNomeXml();
				Fattura fattura = new Fattura();
				fattura.setTipo(Testo.ATTIVA);
				fattura.setTipoDocumento(tipo);
				fattura.setNomeXml(nomeXml);
				fattura.setData(data);
				fattura.setDataCon(dataConsegna);
				fattura.setDataRic(dataRicezione);
				fattura.setNumero(numero);
				fattura.setNumProEq(protocollo);
				fattureAttive.add(fattura);
			}

			
			System.out.println("Inizio import in AS400 fatture attive");
			String uuid = UUID.randomUUID().toString().substring(0,8);
			int row = 0;
			for (Fattura f: fattureAttive){
				row++;
				if (row > 1){
					break;
				}
				System.out.println("Fattura attiva: " + f.getTipo() + "|" +  f.getTipoDocumento() + "|"  + f.getNumero() + "|" +  f.getData());
				Application.getInstance().getDao().insertDataAs400(f, uuid, row);
			}
			System.out.println("Inizio command in AS400 fatture attive");
			if (!fattureAttive.isEmpty()){
				Application.getInstance().getDao().insertCmdAs400(uuid, Testo.CMD_AS400_2);
			}
			System.out.println("Fine import in AS400 fatture attive");



		} catch (Exception e) {
			System.out.println(e);
		}   
	}

}
