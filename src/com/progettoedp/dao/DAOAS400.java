package com.progettoedp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.progettoedp.data.Customer;
import com.progettoedp.data.Dettaglio;
import com.progettoedp.data.Fattura;
import com.progettoedp.data.Fornitore;
import com.progettoedp.data.Ordine;
import com.progettoedp.data.Pagamento;
import com.progettoedp.data.Registrazione;
import com.progettoedp.data.Riepilogo;
import com.progettoedp.main.Application;
import com.progettoedp.testo.Testo;
import com.progettoedp.util.Logger;




public class DAOAS400 {

	private static final Logger logger = Logger.getLogger(DAOAS400.class);
	private static final String TAG = "DAOAS400";

	private static int year1 = Calendar.getInstance().get(Calendar.YEAR);
	private static int year2 = Calendar.getInstance().get(Calendar.YEAR) % 100;

	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");	 
	private static final SimpleDateFormat sdfTime = new SimpleDateFormat("HHmmss");
	
	private static final SimpleDateFormat sdfDate1 = new SimpleDateFormat("dd/MM/yy");
	private static final SimpleDateFormat sdfDate2 = new SimpleDateFormat("dd/MM/yyyy");
			
	private static final String LIBRARY = Application.getInstance().getLibrary_as400();
	private static final String LIBRARY_CONT = Application.getInstance().getLibrary_as400_cont();
	
	private static final List<Customer> customers = Application.getInstance().getCustomers();
	
	// 	private static final String LIBRARY = "IT_SOL_F";

	private static final String TABLE_COMMAND = LIBRARY + ".IECMD00F";
	private static final String ANA_1 = LIBRARY + ".ANACF10F";
	private static final String ANA_2 = LIBRARY + ".ANACF40F";
	private static final String TAB = LIBRARY + ".TBCOM00F";

	private static final String F_CODICE_1 =  "ACODA1";
	private static final String F_CODICE_2 =  "ACODA4";
	private static final String F_RAGIONE_SOCIALE =  "ARAG11";
	private static final String F_RAGIONE_SOCIALE_2 =  "ARAG21";
	private static final String F_SOTTOCONTO_CLIENTE =  "ASTCC1";
	private static final String F_SOTTOCONTO_FORNITORE =  "ASTCF1";
	private static final String F_CLASSE =  "ACCLA4" ;
	private static final String F_DESCRIZIONE =  "TBDESC";
	private static final String F_PARTITA_IVA =  "APAIV1";
	private static final String F_ANNULLA =  "ACANN1";
	private static final String F_DATA_CESSAZIONE =  "ADCES1";
	private static final String F_CODICE_FISCALE =  "ACFIS1";

	private static final String TAB_CONT = LIBRARY_CONT + ".MX_CO00001";
	private static final String F_NUM_PRO =  "NRPROTIVA";
	private static final String F_REG_IVA =  "CDREG00001";
	private static final String F_DATA_REG =  "DTREG";
	private static final String F_ID_SDI =  "IDSDI";

	private static final String F_TPTB =  "TBTPTB";
	private static final String F_CTB1 =  "TBCTB1";
	private static final String F_CTB2 =  "TBCTB2"; 
	private static final String F_CTB3 =  "TBCTB3";
	private static final int MAX_SIZE = 5;
	private static final int MAX_SIZE_SPA = 3;
	private static final int MAX_SIZE_ART = 50;

	public static boolean insertCmd(Connection connection, String uuid, String ccie){

		try{


			boolean execute=true;

			//		      String uuid = UUID.randomUUID().toString().substring(0,8);	  

			String uten = Testo.USER_AS400;
			String lib = Testo.LIB_AS400;
			//		      String ccie = Testo.CMD_AS400;
			String ga = Testo.GA_AS400;
			String utec = Testo.USER_ITACA_AS400;
			String empty = "";

			String dataStr = sdf.format(new Date());
			String timeStr = sdfTime.format(new Date());


			PreparedStatement st = connection.prepareStatement("INSERT INTO " + LIBRARY + ".IECMD00F (ICJOBN, ICCCIE, ICUTEN, ICLGAA, ICLOBJ, ICDTAC, ICORAC, ICUTEC, ICFAS1, ICFAS2, CPCCCHK) VALUES (?,?,?,?,?,?,?,?,?,?,?)");

			st.setString(1, uuid.toUpperCase());
			st.setString(2, ccie.trim());
			st.setString(3, uten.trim());
			st.setString(4, ga.trim());
			st.setString(5, lib.trim());
			st.setInt(6, Integer.parseInt(dataStr));
			st.setInt(7, Integer.parseInt(timeStr));
			st.setString(8, utec.trim());
			st.setString(9, empty);
			st.setString(10, empty);
			st.setString(11, empty);
			execute = st.execute();
			logger.info(TAG, "Command AS400 launched: " + uuid.toUpperCase());

			return execute;


		}

		catch(SQLException e){
			logger.error(TAG, e);
			return false;
		}
	}
	
	public static boolean insertOrdine(Connection connection, String uuid, int row, Ordine ordine){

		try{


			boolean execute=true;

			PreparedStatement st = connection.prepareStatement("INSERT INTO " + LIBRARY + ".IEINP00F (IIJOBN, IIJOBR, IITPRK, IIDATI, IIFSTA, CPCCCHK) VALUES (?,?,?,?,?,?)");

			st.setString(1, uuid.toUpperCase());
			st.setInt(2, row);
			st.setString(3, "NSO");
			st.setString(4, createRigaOrd(ordine));
			st.setString(5, "S");
			st.setString(6, "XXXXXXXXXX");

			execute = st.execute();
			logger.info(TAG, "Order NSO AS400 executed: " + ordine.getIdOrder() +  "|" + ordine.getIdDelivery() + "|" + ordine.getName() + "|" + ordine.getNote());
		
			return execute;

		}

		catch(SQLException e){
			logger.error(TAG, e);
			return false;
		}
	}

	public static boolean insertDati(Connection connection, String uuid, int row, Fattura fattura){

		try{


			boolean execute=true;

			PreparedStatement st = connection.prepareStatement("INSERT INTO " + LIBRARY + ".IEINP00F (IIJOBN, IIJOBR, IITPRK, IIDATI, IIFSTA, CPCCCHK) VALUES (?,?,?,?,?,?)");

			st.setString(1, uuid.toUpperCase());
			st.setInt(2, row);

			if (fattura.getTipo().equals(Testo.PASSIVA)){
				st.setString(3, "ACQ");
				st.setString(4, createRiga(fattura));
			}
			
			String causale = null;
			if(fattura.getCausale() == null){
				causale = Testo.NON_DEFINITO;
			} else {
				if  (fattura.getCausale().trim().length() > 50){
					causale = fattura.getCausale().substring(0, 50);
				} else {
					causale = fattura.getCausale();
				}
			}
			
			
			if (fattura.getTipo().equals(Testo.ATTIVA)){
				if (causale.equals(Testo.SPACCIO)){
					st.setString(3, "DOC");
					st.setString(4, createRigaSpaccio(fattura));
				} else {
					st.setString(3, "DOC");
					st.setString(4, createRigaDoc(fattura));	
				}
			}

			st.setString(5, "S");
			st.setString(6, "XXXXXXXXXX");

			execute = st.execute();
			logger.info(TAG, "Fattura AS400 executed: " +  uuid.toUpperCase() + "|" + fattura.getTipoDocumento() + "|" + fattura.getData() + "|" + fattura.getNumero() +"|" + fattura.getNomeXml());

			return execute;


		}

		catch(SQLException e){
			logger.error(TAG, e);
			return false;
		}
	}

	public static String getValue(String input, int lenght, boolean zeros){

		if (input == null){
			input =  "";
		}

		if  (input.trim().length() > lenght){
			input = input.substring(0, lenght);
		}
		String ouput = null;
		if (!zeros) {
			ouput = String.format("%-" + lenght + "s",input);
		} else {
			ouput = String.format("%" + lenght + "s",input);
			ouput = ouput.replace(" ","0");
		}

		return ouput;
	}

	public static String createRiga(Fattura fattura){

		StringBuilder sb = new StringBuilder();
		String tipo = fattura.getTipoDocumento();
		String numero = fattura.getNumero();
		String data = fattura.getData().substring(0, 10);
		String totale = String.format("%.2f", fattura.getTotaleFattura()).replaceAll(",", "");
		String partitaIva = fattura.getPartitaIva();
		String codiceFiscale = fattura.getCodiceFiscale();
		String numeroAs400 = getNumeroAs400(partitaIva, numero);
		String nomeXml = fattura.getNomeXml();
		String bolloVirtuale = fattura.getBolloVirtuale();
		String importoBollo = String.format("%.2f", fattura.getImportoBollo()).replaceAll(",", "");
		
		String fornitore = null;
		if (fattura.getRagioneSociale()!= null){
			fornitore = fattura.getRagioneSociale();
		} else if(fattura.getNome() != null && fattura.getCognome() != null){
			fornitore = fattura.getNome() + " " + fattura.getCognome();
		}

		String dataRegistrazione = fattura.getDataRitiro();
		String protocollo = fattura.getIdSdi();

		String dataProtocolloEq = null;
		if (fattura.getDataProEq()!= null){
			dataProtocolloEq = fattura.getDataProEq();
		}
		String protocolloEq = null;
		if (fattura.getNumProEq()!= null){
			protocolloEq = fattura.getNumProEq();
		}

		sb.append(getValue(tipo, 4, false));
		sb.append(getValue(numero, 20, false));
		sb.append(getValue(data, 10, false));
		sb.append(getValue(dataRegistrazione, 10, false));
		sb.append(getValue(protocollo.trim(), 20, false));
		sb.append(getValue(dataProtocolloEq, 10, false));
		sb.append(getValue(protocolloEq, 20, false));
		sb.append(getValue(partitaIva, 11, false));
		sb.append(getValue(fornitore.trim(), 50, false));
		sb.append(getValue(totale, 13, true));

		List<Riepilogo> riepiloghi = fattura.getRiepiloghi();
		int size = riepiloghi.size();
		for (Riepilogo r: riepiloghi){
			String imponibile = String.format("%.2f", r.getImponibile()).replaceAll(",", "");
			String imposta = String.format("%.2f", r.getImposta()).replaceAll(",", "");
			String iva = r.getIva().split("\\.")[0];
			String natura = r.getNatura();
			sb.append(getValue(imponibile, 13, true));
			sb.append(getValue(imposta, 11, true));
			sb.append(getValue(iva, 2, true).replaceAll("\\.", ""));
//			sb.append(getValue(natura, 2, false).replaceAll("\\.", ""));
			sb.append(getValue(natura, 4, false));
		}

		int result = MAX_SIZE-size;
		if (result != 0){
			for (int i = 0; i < result; i++) {
				sb.append(getValue("0", 13, true));
				sb.append(getValue("0", 11, true));
				sb.append(getValue("", 2, true).replaceAll("\\.", ""));
				sb.append(getValue("", 4, false));
			}
		}


		List<Pagamento> pagamenti = fattura.getPagamenti();
		size = pagamenti.size();
		for (Pagamento p: pagamenti){
			String modalita = p.getModalita();
			String dataScadenza = p.getScadenza();
			String importo = String.format("%.2f", p.getImporto()).replaceAll(",", "");
			sb.append(getValue(modalita, 4, false));
			sb.append(getValue(dataScadenza, 10, true));
			sb.append(getValue(importo, 13, true).replaceAll("\\.", ""));
		}

		result = MAX_SIZE-size;
		if (result != 0){
			for (int i = 0; i < result; i++) {
				sb.append(getValue("0", 4, false));
				sb.append(getValue("0", 10, true));
				sb.append(getValue("", 13, true).replaceAll("\\.", ""));
			}
		}

		sb.append(getValue(numeroAs400, 20, false));
		sb.append(getValue(codiceFiscale, 16, false));
		sb.append(getValue(nomeXml.trim(), 70, false));
		sb.append(getValue(bolloVirtuale, 2, false));
		sb.append(getValue(importoBollo, 13, true));
		
		logger.info(TAG, "Fattura passive AS400: " + numero + "|" + data + "|" + numeroAs400 + "|" + nomeXml);
		return sb.toString();

	}
	
	public static String createRigaOrd(Ordine ordine){

		StringBuilder sb = new StringBuilder();
		String tipo = Testo.ORDINE_NSO;
		String numero = ordine.getIdOrder();
		
		String data = ordine.getIssueDate().substring(0, 10);
		String partitaIva = ordine.getPartitaIva().replace("IT", "");
		String name = ordine.getName();
		String nomeXml = ordine.getNomeXml();
		String cityName = ordine.getCityName();
		
		String idDelivery=null;
		try {
			idDelivery = ordine.getIdDelivery().split("-")[1];
		} catch (Exception e) {
			idDelivery = ordine.getIdDelivery();
		}
		
		sb.append(getValue(tipo, 4, false));
		sb.append(getValue(numero, 20, false));
		sb.append(getValue(data, 10, false));
		sb.append(getValue(partitaIva, 11, false));
		sb.append(getValue(name, 30, false));
		sb.append(getValue(cityName, 30, false));
		sb.append(getValue(idDelivery.trim(), 20, false));
		sb.append(getValue(nomeXml.trim(), 70, false));

		return sb.toString();
	}

		 

	public static String createRigaDoc(Fattura fattura){

		StringBuilder sb = new StringBuilder();
		String tipo = fattura.getTipoDocumento();
		String numero = fattura.getNumero();
		String data = fattura.getData().substring(0, 10);
		String dataRicezione = fattura.getDataRic();
		String dataConsegna = null;
		if (fattura.getDataCon() != null){
			dataConsegna = fattura.getDataCon();
		} else {
			dataConsegna = "";
		}

		String protocollo = fattura.getIdSdi();
		String nomeXml = fattura.getNomeXml();
		String statoSdi = null;
		if (fattura.getStatoSdi() != null){
			statoSdi = fattura.getStatoSdi();
		} else {
			statoSdi = "";
		}

		sb.append(getValue(tipo, 4, false));
		sb.append(getValue(numero, 20, false));
		sb.append(getValue(data, 10, false));
		sb.append(getValue(dataRicezione, 20, false));
		sb.append(getValue(dataConsegna, 20, false));
		sb.append(getValue(protocollo.trim(), 20, false));
		sb.append(getValue(nomeXml.trim(), 70, false));
		sb.append(getValue(statoSdi.trim(), 70, false));
		return sb.toString();

	}
	
	public static String createRigaSpaccio(Fattura fattura){

		StringBuilder sb = new StringBuilder();
		String tipo = fattura.getTipoDocumento();
		String numero = fattura.getNumero();
		String data = fattura.getData().substring(0, 10);
		String totale = String.format("%.2f", fattura.getTotaleFattura()).replaceAll(",", "");
		String partitaIvaCli = fattura.getPartitaIvaCliente();
		String codiceFiscaleCli = fattura.getCodiceFiscaleCliente();

		 
		String dataRegistrazione = fattura.getDataRitiro();
		String protocollo = fattura.getIdSdi();
 
		sb.append(getValue(tipo, 4, false));
		sb.append(getValue(numero, 20, false));
		sb.append(getValue(data, 10, false));
		sb.append(getValue(dataRegistrazione, 10, false));
		sb.append(getValue(protocollo.trim(), 20, false));
		sb.append(getValue(partitaIvaCli, 11, false));
		sb.append(getValue(codiceFiscaleCli, 16, false));
		sb.append(getValue(totale, 9, true));
		
		List<Dettaglio> dettagli = fattura.getDettagli();
		List<String> articoli = new ArrayList<String>();
		List<Double> importi = new ArrayList<Double>();
		List<Double> quantita = new ArrayList<Double>();
		for (Dettaglio d: dettagli){
			articoli.add(d.getArticolo());
			importi.add(d.getPrezzoTotale());
			quantita.add(d.getQuantita());
		}
		int size = articoli.size();
		for (String articolo: articoli){
			sb.append(getValue(articolo, 5, false));
		}
		
	 	int result = MAX_SIZE_ART-size;
		if (result != 0){
			for (int i = 0; i < result; i++) {
				sb.append(getValue("", 5, false).replaceAll("\\.", ""));
			}
		}
		
		for (Double importo: importi){
			String imponibile = String.format("%.2f", importo).replaceAll(",", "");
			sb.append(getValue(imponibile, 7, true));
		}
	 	 
		if (result != 0){
			for (int i = 0; i < result; i++) {
				sb.append(getValue("0", 7, true));
			}
		}
		
		for (Double q: quantita){
			String quant = String.format("%.2f", q).replaceAll(",", "");
			sb.append(getValue(quant, 5, true));
		}
	 	 
		if (result != 0){
			for (int i = 0; i < result; i++) {
				sb.append(getValue("0", 5, true));
			}
		}
		

		List<Riepilogo> riepiloghi = fattura.getRiepiloghi();
		size = riepiloghi.size();
		for (Riepilogo r: riepiloghi){
			String imponibile = String.format("%.2f", r.getImponibile()).replaceAll(",", "");
			String imposta = String.format("%.2f", r.getImposta()).replaceAll(",", "");
			String iva = r.getIva().split("\\.")[0];
			String natura = r.getNatura();
			sb.append(getValue(imponibile, 7, true));
			sb.append(getValue(imposta, 5, true));
			sb.append(getValue(iva, 2, true).replaceAll("\\.", ""));
			sb.append(getValue(natura, 2, false).replaceAll("\\.", ""));
		}

		result = MAX_SIZE_SPA-size;
		if (result != 0){
			for (int i = 0; i < result; i++) {
				sb.append(getValue("0", 7, true));
				sb.append(getValue("0", 5, true));
				sb.append(getValue("", 2, true).replaceAll("\\.", ""));
				sb.append(getValue("", 2, false).replaceAll("\\.", ""));
			}
		}
 
		logger.info(TAG, "Fattura spaccio AS400: " + numero + "|" + data);
		return sb.toString();

	}

	public static Fornitore readFornitore(Connection connection, String partitaIva, String codiceFiscale){

		try{

			PreparedStatement st =null; 
			String select = null;
			
			if (codiceFiscale==null || partitaIva.equals(codiceFiscale)){
				select = "SELECT " +
						F_CODICE_1 +", " +
						F_RAGIONE_SOCIALE+", " +
						F_RAGIONE_SOCIALE_2+", " +
						F_SOTTOCONTO_FORNITORE+", " +
						F_PARTITA_IVA+", " +
						F_CLASSE+", " +
						F_DESCRIZIONE+" " +
						" FROM "+ ANA_1 + "," + ANA_2 +"," + TAB +
						" WHERE " + F_ANNULLA + "<>" + "'A'" +
						" AND " + F_DATA_CESSAZIONE + "= " + 0 +  
						" AND " + F_CODICE_1 + "=" + F_CODICE_2 +    
						" AND " + F_PARTITA_IVA + "=" + partitaIva.trim() +  
						" AND " + F_TPTB + "=" + "'CLF'" +
						" AND " + F_CTB1 + "=" + F_CLASSE;
			} else {
				select = "SELECT " +
						F_CODICE_1 +", " +
						F_RAGIONE_SOCIALE+", " +
						F_RAGIONE_SOCIALE_2+", " +
						F_SOTTOCONTO_FORNITORE+", " +
						F_PARTITA_IVA+", " +
						F_CLASSE+", " +
						F_DESCRIZIONE+" " +
						" FROM "+ ANA_1 + "," + ANA_2 +"," + TAB +
						" WHERE " + F_ANNULLA + "<>" + "'A'" +
						" AND " + F_DATA_CESSAZIONE + "= " + 0 +  
						" AND " + F_CODICE_1 + "=" + F_CODICE_2 +    
						" AND " + F_PARTITA_IVA + "=" + partitaIva.trim() +
						" AND " + F_CODICE_FISCALE + "=" + "'" + codiceFiscale.trim() + "'"+
						" AND " + F_TPTB + "=" + "'CLF'" +
						" AND " + F_CTB1 + "=" + F_CLASSE;
			}
			

			logger.debug(TAG, "Query AS400 trovato: " + select);
 
			st = connection.prepareStatement(select);

			ResultSet rs = st.executeQuery();
			Fornitore fornitore = new Fornitore();

			while(rs.next()){
				String codice = rs.getString(1);
				String ragioneSociale = rs.getString(2);
				String ragioneSociale2 = rs.getString(3);
				int sottoconto = rs.getInt(4);
				String iva = rs.getString(5);
				String classe = rs.getString(6);
				String descrizione = rs.getString(7);

				fornitore.setCodice(codice);
				fornitore.setPartitaIva(iva);
				fornitore.setRagioneSociale(ragioneSociale+" " +ragioneSociale2);
				fornitore.setSottoconto(sottoconto);
				fornitore.setClasse(classe);
				fornitore.setDescrizione(descrizione);
			}

			rs.close();

			if (fornitore.getDescrizione()!= null){
				logger.debug(TAG, "Fornitore AS400 trovato: " + fornitore.getCodice() + "|" + fornitore.getRagioneSociale() + "|" + fornitore.getDescrizione());
			} else {
				logger.debug(TAG, "Fornitore AS400 non trovato: " + partitaIva);
			}

			return fornitore;

		}

		catch(SQLException e){
			logger.error(TAG, e);
			return null;
		}
	}

	public static Registrazione readProtocollo(Connection connection, String idSdi){

		try{

			PreparedStatement st =null; 

			String select = "SELECT " +
					F_NUM_PRO +",  " +
					F_REG_IVA +",  " +
					F_DATA_REG +"  " +
					" FROM "+ TAB_CONT +
					" WHERE " + F_ID_SDI + "=" + "'" + idSdi + "'";


			st = connection.prepareStatement(select);

			ResultSet rs = st.executeQuery();
			Registrazione registrazione = new Registrazione();

			while(rs.next()){
				String numeroProtocollo = String.valueOf(rs.getInt(1));
				String registroIva = rs.getString(2);
				String dataRegistrazione = reformatData(sdfDate1, rs.getString(3));
				
				registrazione.setNumeroProtocollo(numeroProtocollo);
				registrazione.setRegistroIva(registroIva);
				registrazione.setDataRegistrazione(dataRegistrazione);
				logger.info(TAG, "Debug protocollo: " + registrazione.toString());
			}

			rs.close();

			if (registrazione.getNumeroProtocollo() != null){
				logger.debug(TAG, "Protocollo AS400 trovato: " + registrazione.getNumeroProtocollo());
			} else {
				logger.debug(TAG, "Protocollo AS400 non trovato: " + idSdi);
			}

			return registrazione;

		}

		catch(SQLException e){
			logger.error(TAG, e);
			return null;
		}
	}

	public static String getNumeroAs400(String partitaIva, String numero) {
		
		if (customers!=null){
			String numeroCustom = getNumeroCustom(partitaIva, numero);
			if (numeroCustom!=null){
				return numeroCustom;
			}
		}
		

		try {

			String separator = "";
			if (numero.contains("/")) {
				separator = "/";
			} else if (numero.contains("-")) {
				separator = "-";
			} else if (numero.contains("|")) {
				separator = "|";
			}

			String[] array = numero.split("\\" + separator, -1);
			String newNum = "";
			for (String s: array) {     

				try {
					if (Integer.parseInt(s) == year1 || Integer.parseInt(s) == year2) {
						continue;
					}

				} catch (Exception e) {

				}

				newNum=newNum.trim()+s;
			}

			newNum = newNum.replaceAll("[^0-9]", "");
			logger.debug(TAG, "Numero AS400 convertito: " + newNum);
			
			return newNum;


		}
		catch (Exception e) {
			return "";
		}
		
		

	}
	
	public static String getNumeroCustom(String partitaIva, String numero) {
		
		String numeroCustom = null;
		
		try {

			for (Customer c: customers){
				
				if (c.getPartitaIva().equals(partitaIva)){
					
					String separatore = c.getSeparatore();
					String allineamento = c.getAllineamento();
					int occorrennza = c.getOccorrenza();
					int separatorLen = separatore.length();
//					int indexOf = numero.indexOf(separatore);
					
					int indexOf = StringUtils.ordinalIndexOf(numero, separatore, occorrennza);
					
					switch (allineamento) {
					case Testo.DESTRA:
						numeroCustom = numero.substring(indexOf+separatorLen, numero.length());
						break;
					case Testo.SINISTRA:
						numeroCustom = numero.substring(0, indexOf);
						break;

					default:
						break;
					}
					logger.debug(TAG, "Numero custom cliente trovato: " + partitaIva + "|" + separatore + "|" + allineamento + "|" + numeroCustom);
					return numeroCustom;
					
				}
			}
		} catch (Exception e) {
			 
		}
		
		
		return numeroCustom;
		
	}
	
	public static String reformatData(SimpleDateFormat fromFormat, String originDate){
		String reformatteDate = null;
		try {
			reformatteDate = sdfDate2.format(fromFormat.parse(originDate.trim()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return reformatteDate;
	}

}
