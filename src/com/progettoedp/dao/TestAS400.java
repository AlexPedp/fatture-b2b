package com.progettoedp.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.progettoedp.data.Registrazione;
import com.progettoedp.testo.Testo;

public class TestAS400 {

	private static final String TAB_CONT =  "BMS60L_DAT.MX_CO00001";
	private static final String F_NUM_PRO =  "NRPROTIVA";
	private static final String F_REG_IVA =  "CDREG00001";
	private static final String F_ID_SDI =  "IDSDI";

	public static void main(String[] args)  {

		System.out.println("-------- MySQL JDBC Connection Testing ------------");

		try {
			Class.forName("com.ibm.as400.access.AS400JDBCDriver");
		} catch (ClassNotFoundException e) {

			System.out.println("Where is your AS400 JDBC Driver?");
			e.printStackTrace();
			return;
		}

		System.out.println("AS400 JDBC Driver Registered!");
		Connection connection = null;


		try {
			String host_as400 = Testo.IP_AS400;
			String user_as400 = Testo.USER_AS400;
			String password_as400 = Testo.PWD_AS400;
			String idSdi = "2142499900";

			connection = DriverManager.getConnection("jdbc:as400://" + host_as400, user_as400, password_as400);



			PreparedStatement st =null; 

			String select = "SELECT " +
					F_NUM_PRO +",  " +
					F_REG_IVA +"  " +
					" FROM "+ TAB_CONT +
					" WHERE " + F_ID_SDI + "=" + "'" + idSdi + "'";


			st = connection.prepareStatement(select);

			ResultSet rs = st.executeQuery();
			Registrazione registrazione = new Registrazione();

			while(rs.next()){
				String numeroProtocollo = String.valueOf(rs.getInt(1));
				String registroIva = rs.getString(2);
				registrazione.setNumeroProtocollo(numeroProtocollo);
				registrazione.setRegistroIva(registroIva);
			}

			rs.close();

			if (registrazione.getNumeroProtocollo() != null){
				System.out.println("Protocollo AS400 trovato: " + registrazione.getNumeroProtocollo() + "|" + registrazione.getRegistroIva());
			} else {
				System.out.println("Protocollo AS400 trovato: " + idSdi);
			}




		} catch (Exception e) {
			System.out.println(e);
		}

	}

}
