package com.progettoedp.dao;

import java.lang.reflect.Type;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.progettoedp.data.Dettaglio;
import com.progettoedp.data.Fattura;
import com.progettoedp.data.Ordine;
import com.progettoedp.data.Pagamento;
import com.progettoedp.data.Riepilogo;
import com.progettoedp.testo.Testo;
import com.progettoedp.util.Logger;


public class DAOFatture {

	private static final Logger logger = Logger.getLogger(DAOFatture.class);
	private static final String TAG = DAOFatture.class.getCanonicalName();

	private static final String TABLE = "FATTURE";

	private static final String F_ID = "ID";
	private static final String F_ATT_PASS = "ATT_PASS";
	private static final String F_TIPO="TIPO";
	private static final String F_NUMERO="NUMERO";
	private static final String F_DATA="DATA";
	private static final String F_PARTITA_IVA="PARTITA_IVA";
	private static final String F_RAGIONE_SOCIALE="RAGIONE_SOCIALE";
	private static final String F_TOTALE_FATTURA="TOTALE";
	private static final String F_ALLEGATO="TOTALE";
	private static final String F_DATA_RITIRO="DATA_RITIRO";
	private static final String F_DATA_SDI_1="DATA_SDI_1";
	private static final String F_DATA_SDI_2="DATA_SDI_2";
	private static final String F_ID_SDI="ID_SDI";
	private static final String F_NOME_XML="NOME_XML";
	private static final String F_DETTAGLI="DETTAGLI";
	private static final String F_RIEPILOGHI="RIEPILOGHI";
	private static final String F_PAGAMENTI="PAGAMENTI";
	private static final String F_NUM_PRO_EQ="NUMERO_PRO_EQ";
	private static final String F_DATA_PRO_EQ="DATA_PRO_EQ";
	private static final String F_DATA_RIC="DATA_RICEZIONE";
	private static final String F_DATA_CON ="DATA_CONSEGNA";
	private static final String F_STATO ="STATO";
	private static final String F_STATO_SDI ="STATO_SDI";
	private static final String F_REG_FISCALE ="REG_FISCALE";
	private static final String F_CAUSALE ="CAUSALE";
	private static final String F_PARTITA_IVA_CLI ="PARTITA_IVA_CLI";
	private static final String F_CODICE_FISCALE_CLI ="CODICE_FISCALE_CLI";
	private static final String F_BOLLO_VIRTUALE ="BOLLO_VIRTUALE";
	private static final String F_IMPORT_BOLLO ="IMPORTO_BOLLO";


	public static Fattura findFatturaByNumero(Connection connection, String tipo, String tipDoc, String datDoc, String numDoc, String piva){

		try{
			PreparedStatement st =null;

			String select = "SELECT " +
					F_ID+", "+
					F_ATT_PASS+", "+
					F_TIPO+", " +
					F_NUMERO+", " +
					F_DATA  + ", " + 
					F_PARTITA_IVA  + ", " +
					F_RAGIONE_SOCIALE  + ", " +
					F_TOTALE_FATTURA  + ", " +
					F_ALLEGATO  + ", " +
					F_DATA_RITIRO  + ", " +
					F_DATA_SDI_1  + ", " +
					F_DATA_SDI_2  + ", " +
					F_ID_SDI  + ", " +
					F_NOME_XML  + ", " +
					F_NUM_PRO_EQ + ", " +
					F_DATA_PRO_EQ + ", " +
					F_DATA_RIC + ", " +
					F_DATA_CON + ", " +
					F_STATO_SDI + ", " +
					F_DETTAGLI  + ", " +
					F_RIEPILOGHI + ", " +
					F_PAGAMENTI + " " +
					" FROM "+TABLE + " WHERE " + F_NUMERO + "='" + numDoc + "'" + 
					" AND "+ F_ATT_PASS + "='" + tipo + "'" +
					" AND "+ F_TIPO + "='" + tipDoc + "'" +
					" AND "+ F_DATA + "='" + datDoc + "'" +
					" AND "+ F_PARTITA_IVA + "='" + piva + "'" +
					" ORDER BY " + F_ID; 

			st = connection.prepareStatement(select);

			ResultSet rs = st.executeQuery();

			while(rs.next()){
				Long id = rs.getLong(1);
				String att_pass = rs.getString(2);
				String tipoF = rs.getString(3);
				String numero = rs.getString(4);
				String data = rs.getString(5);
				String partitaIva = rs.getString(6);
				String ragioneSociale = rs.getString(7);
				Double totaleFattura = rs.getDouble(8);
				String allegato = rs.getString(9);
				String dataRitiro = rs.getString(10);
				String dataSdi1 = rs.getString(11);
				String dataSdi2 = rs.getString(12);
				String idSdi = rs.getString(13);
				String nomeXml = rs.getString(14);
				String numProEq = rs.getString(15);
				String dataProEq = rs.getString(16);
				String dataRic = rs.getString(17);
				String dataCon = rs.getString(18);
				String statoSdi = rs.getString(19);
				String dettagliJson = rs.getString(20);
				String riepiloghiJson = rs.getString(21);
				String pagamentiJson = rs.getString(22);

				Type typeDettaglio = new TypeToken<ArrayList<Dettaglio>>(){}.getType();
				List<Dettaglio> dettagli = new Gson().fromJson(dettagliJson, typeDettaglio);

				Type typeRiepilogo = new TypeToken<ArrayList<Riepilogo>>(){}.getType();
				List<Riepilogo> riepiloghi = new Gson().fromJson(riepiloghiJson, typeRiepilogo);

				Type typePagamento = new TypeToken<ArrayList<Pagamento>>(){}.getType();
				List<Pagamento> pagamenti = new Gson().fromJson(pagamentiJson, typePagamento);
				
				List<String> allegati = new ArrayList<String>();
				try {
					Type typeAllegato = new TypeToken<ArrayList<String>>(){}.getType();
					allegati = new Gson().fromJson(allegato, typeAllegato);
				} catch (Exception e) {
					allegati.add(new Gson().toJson(allegato));  
				}
			
				Fattura fattura = new Fattura();
				fattura.setTipo(att_pass);
				fattura.setTipoDocumento(tipoF);
				fattura.setNumero(numero);
				fattura.setData(data);
				fattura.setPartitaIva(partitaIva);
				fattura.setRagioneSociale(ragioneSociale);
				fattura.setTotaleFattura(totaleFattura);
				fattura.setAllegato(allegati);
				fattura.setDataRitiro(dataRitiro);
				fattura.setDataSdi1(dataSdi1);
				fattura.setDataSdi2(dataSdi2);
				fattura.setIdSdi(idSdi);
				fattura.setNomeXml(nomeXml);
				fattura.setNumProEq(numProEq);
				fattura.setDataProEq(dataProEq);
				fattura.setDataRic(dataRic);
				fattura.setDataCon(dataCon);
				fattura.setStatoSdi(statoSdi);
				fattura.setDettagli(dettagli);
				fattura.setRiepiloghi(riepiloghi);
				fattura.setPagamenti(pagamenti);
				return fattura;

			}
			rs.close();

		}catch (SQLException e) {
			logger.error(TAG,e);
		}
		return null;
	}
	
	public static Ordine findOrdineByNumero(Connection connection, String tipo, String tipDoc, String datDoc, String numDoc, String partitaIva){

		try{
			PreparedStatement st =null;

			String select = "SELECT " +
					F_ID+", "+
					F_ATT_PASS+", "+
					F_TIPO+", " +
					F_NUMERO+", " +
					F_DATA  + " " + 
					" FROM "+TABLE + " WHERE " + F_NUMERO + "='" + numDoc + "'" + 
					" AND "+ F_ATT_PASS + "='" + tipo + "'" +
					" AND "+ F_TIPO + "='" + tipDoc + "'" +
					" AND "+ F_DATA + "='" + datDoc + "'" +
					" ORDER BY " + F_ID; 

			st = connection.prepareStatement(select);

			ResultSet rs = st.executeQuery();

			while(rs.next()){
				Long id = rs.getLong(1);
				String att_pass = rs.getString(2);
				String tipoF = rs.getString(3);
				String numero = rs.getString(4);
				String data = rs.getString(5);
			
				Ordine ordine = new Ordine();
				ordine.setIssueDate(data);
				ordine.setIdOrder(numero);
				return ordine;

			}
			rs.close();

		}catch (SQLException e) {
			logger.error(TAG,e);
		}
		return null;
	}

	public static List<Fattura> findfattureByTipoAndStato(Connection connection, String tipoAttPass, String stato, boolean spaccio, String type){

		List<Fattura> fatture = new ArrayList<Fattura>();

		try{
			PreparedStatement st =null;

			String select = "SELECT " +
					F_ID+", "+
					F_ATT_PASS+", "+
					F_TIPO+", " +
					F_NUMERO+", " +
					F_DATA  + ", " + 
					F_PARTITA_IVA  + ", " +
					F_RAGIONE_SOCIALE  + ", " +
					F_TOTALE_FATTURA  + ", " +
					F_ALLEGATO  + ", " +
					F_DATA_RITIRO  + ", " +
					F_DATA_SDI_1  + ", " +
					F_DATA_SDI_2  + ", " +
					F_ID_SDI  + ", " +
					F_NOME_XML  + ", " +
					F_NUM_PRO_EQ + ", " +
					F_DATA_PRO_EQ + ", " +
					F_DATA_RIC + ", " +
					F_DATA_CON + ", " +
					F_STATO_SDI + ", " +
					F_CAUSALE + " , "  +
					F_DETTAGLI  + ", " +
					F_RIEPILOGHI + ", " +
					F_PAGAMENTI + " " +
					" FROM "+TABLE + " WHERE " + F_ATT_PASS + "='" + tipoAttPass + "'" + 
					" AND " + F_STATO + " = '" +  stato + "'" + 
					" ORDER BY " + F_ID; 

			System.out.println("Select: " + select);

			st = connection.prepareStatement(select);

			ResultSet rs = st.executeQuery();

			while(rs.next()){
				Long id = rs.getLong(1);
				String att_pass = rs.getString(2);
				String tipo = rs.getString(3);
				String numero = rs.getString(4);
				String data = rs.getString(5);
				String partitaIva = rs.getString(6);
				String ragioneSociale = rs.getString(7);
				Double totaleFattura = rs.getDouble(8);
				String allegato = rs.getString(9);
				String dataRitiro = rs.getString(10);
				String dataSdi1 = rs.getString(11);
				String dataSdi2 = rs.getString(12);
				String idSdi = rs.getString(13);
				String nomeXml = rs.getString(14);
				String numProEq = rs.getString(15);
				String dataProEq = rs.getString(16);
				String dataRic = rs.getString(17);
				String dataCon = rs.getString(18);
				String statoSdi = rs.getString(19);
				String causale = rs.getString(20);
				String dettagliJson = rs.getString(21);
				String riepiloghiJson = rs.getString(22);
				String pagamentiJson = rs.getString(23);
				
				
				switch (type) {
				case Testo.REIMPORT:
					if (tipo.equals(Testo.ATTIVA)){
						if (!spaccio && causale.equals(Testo.SPACCIO)){
							continue;
						} 
						if (spaccio && !causale.equals(Testo.SPACCIO)){
							continue;
						} 
					}
					break;
					
				case Testo.NOT_IMPORTED:
					if (!att_pass.equals(Testo.ATTIVA)){
						continue;
					}
					break;

				default:
					break;
				}
				
				
				
				Type typeDettaglio = new TypeToken<ArrayList<Dettaglio>>(){}.getType();
				List<Dettaglio> dettagli = new Gson().fromJson(dettagliJson, typeDettaglio);

				Type typeRiepilogo = new TypeToken<ArrayList<Riepilogo>>(){}.getType();
				List<Riepilogo> riepiloghi = new Gson().fromJson(riepiloghiJson, typeRiepilogo);

				Type typePagamento = new TypeToken<ArrayList<Pagamento>>(){}.getType();
				List<Pagamento> pagamenti = new Gson().fromJson(pagamentiJson, typePagamento);
				
				List<String> allegati = new ArrayList<String>();
				try {
					Type typeAllegato = new TypeToken<ArrayList<String>>(){}.getType();
					allegati = new Gson().fromJson(allegato, typeAllegato);
				} catch (Exception e) {
					allegati.add(new Gson().toJson(allegato));  
				}

				Fattura fattura = new Fattura();
				fattura.setTipo(att_pass);
				fattura.setTipoDocumento(tipo);
				fattura.setNumero(numero);
				fattura.setData(data);
				fattura.setPartitaIva(partitaIva);
				fattura.setRagioneSociale(ragioneSociale);
				fattura.setTotaleFattura(totaleFattura);
				fattura.setAllegato(allegati);
				fattura.setDataRitiro(dataRitiro);
				fattura.setDataSdi1(dataSdi1);
				fattura.setDataSdi2(dataSdi2);
				fattura.setIdSdi(idSdi);
				fattura.setStatoSdi(statoSdi);
				fattura.setNomeXml(nomeXml);
				fattura.setNumProEq(numProEq);
				fattura.setDataProEq(dataProEq);
				fattura.setDataRic(dataRic);
				fattura.setDataCon(dataCon);
				fattura.setDettagli(dettagli);
				fattura.setRiepiloghi(riepiloghi);
				fattura.setPagamenti(pagamenti);
				fatture.add(fattura);

			}
			rs.close();

			return fatture;

		}catch (SQLException e) {
			logger.error(TAG,e);
		}
		return null;
	}
	
	public static List<Fattura> findAllFatture(Connection connection, int limit, int offset){

		List<Fattura> fatture = new ArrayList<Fattura>();
		
		String parameters = "LIMIT " + limit;
		
		if (offset > 0){
			 parameters = parameters.trim() + " OFFSET " + offset;
		}

		try{
			PreparedStatement st =null;

			String select = "SELECT " +
					F_ID+", "+
					F_ATT_PASS+", "+
					F_TIPO+", " +
					F_NUMERO+", " +
					F_DATA  + ", " + 
					F_PARTITA_IVA  + ", " +
					F_RAGIONE_SOCIALE  + ", " +
					F_TOTALE_FATTURA  + ", " +
					F_ALLEGATO  + ", " +
					F_DATA_RITIRO  + ", " +
					F_DATA_SDI_1  + ", " +
					F_DATA_SDI_2  + ", " +
					F_ID_SDI  + ", " +
					F_NOME_XML  + ", " +
					F_NUM_PRO_EQ + ", " +
					F_DATA_PRO_EQ + ", " +
					F_DATA_RIC + ", " +
					F_DATA_CON + ", " +
					F_STATO_SDI + ", " +
					F_DETTAGLI  + ", " +
					F_RIEPILOGHI + ", " +
					F_PAGAMENTI + " " +
					" FROM "+TABLE +
					" ORDER BY " + F_ID +  " " + parameters; 

			System.out.println("Select: " + select);

			st = connection.prepareStatement(select);

			ResultSet rs = st.executeQuery();

			while(rs.next()){
				Long id = rs.getLong(1);
				String att_pass = rs.getString(2);
				String tipo = rs.getString(3);
				String numero = rs.getString(4);
				String data = rs.getString(5);
				String partitaIva = rs.getString(6);
				String ragioneSociale = rs.getString(7);
				Double totaleFattura = rs.getDouble(8);
				String allegato = rs.getString(9);
				String dataRitiro = rs.getString(10);
				String dataSdi1 = rs.getString(11);
				String dataSdi2 = rs.getString(12);
				String idSdi = rs.getString(13);
				String nomeXml = rs.getString(14);
				String numProEq = rs.getString(15);
				String dataProEq = rs.getString(16);
				String dataRic = rs.getString(17);
				String dataCon = rs.getString(18);
				String statoSdi = rs.getString(19);
				String dettagliJson = rs.getString(20);
				String riepiloghiJson = rs.getString(21);
				String pagamentiJson = rs.getString(22);

				Type typeDettaglio = new TypeToken<ArrayList<Dettaglio>>(){}.getType();
				List<Dettaglio> dettagli = new Gson().fromJson(dettagliJson, typeDettaglio);

				Type typeRiepilogo = new TypeToken<ArrayList<Riepilogo>>(){}.getType();
				List<Riepilogo> riepiloghi = new Gson().fromJson(riepiloghiJson, typeRiepilogo);

				Type typePagamento = new TypeToken<ArrayList<Pagamento>>(){}.getType();
				List<Pagamento> pagamenti = new Gson().fromJson(pagamentiJson, typePagamento);
				
				List<String> allegati = new ArrayList<String>();
				try {
					Type typeAllegato = new TypeToken<ArrayList<String>>(){}.getType();
					allegati = new Gson().fromJson(allegato, typeAllegato);
				} catch (Exception e) {
					allegati.add(new Gson().toJson(allegato));  
				}

				Fattura fattura = new Fattura();
				fattura.setTipo(att_pass);
				fattura.setTipoDocumento(tipo);
				fattura.setNumero(numero);
				fattura.setData(data);
				fattura.setPartitaIva(partitaIva);
				fattura.setRagioneSociale(ragioneSociale);
				fattura.setTotaleFattura(totaleFattura);
				fattura.setAllegato(allegati);
				fattura.setDataRitiro(dataRitiro);
				fattura.setDataSdi1(dataSdi1);
				fattura.setDataSdi2(dataSdi2);
				fattura.setIdSdi(idSdi);
				fattura.setStatoSdi(statoSdi);
				fattura.setNomeXml(nomeXml);
				fattura.setNumProEq(numProEq);
				fattura.setDataProEq(dataProEq);
				fattura.setDataRic(dataRic);
				fattura.setDataCon(dataCon);
				fattura.setDettagli(dettagli);
				fattura.setRiepiloghi(riepiloghi);
				fattura.setPagamenti(pagamenti);
				fatture.add(fattura);

			}
			rs.close();

			return fatture;

		}catch (SQLException e) {
			logger.error(TAG,e);
		}
		return null;
	}


	public static boolean insert(Connection connection, Fattura testata){

		try{

			boolean execute=true;

			String ragioneSociale = null;
			if(testata.getRagioneSociale() != null){
				ragioneSociale = testata.getRagioneSociale();
			} else {
				ragioneSociale = testata.getNome() + " " + testata.getCognome();
			}
			
			String causale = null;
			if(testata.getCausale() == null){
				causale = Testo.NON_DEFINITO;
			} else {
				if  (testata.getCausale().trim().length() > 50){
					causale = testata.getCausale().substring(0, 50);
				} else {
					causale = testata.getCausale();
				}
			}
			
			PreparedStatement st = connection.prepareStatement("INSERT INTO FATTURE (ATT_PASS, TIPO, NUMERO, DATA, PARTITA_IVA, RAGIONE_SOCIALE, TOTALE, ALLEGATO, DETTAGLI, RIEPILOGHI, PAGAMENTI, DATA_RITIRO, DATA_SDI_1, DATA_SDI_2, ID_SDI, STATO_SDI, NOME_XML, "
					+ "NUMERO_PRO_EQ, DATA_PRO_EQ, DATA_RICEZIONE, DATA_CONSEGNA, STATO, REG_FISCALE, CAUSALE, PARTITA_IVA_CLI, CODICE_FISCALE_CLI, BOLLO_VIRTUALE, IMPORTO_BOLLO)"
					+ " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			st.setString(1,testata.getTipo());
			st.setString(2, testata.getTipoDocumento());
			st.setString(3, testata.getNumero());
			st.setString(4,testata.getData());
			st.setString(5,testata.getPartitaIva());
			st.setString(6,ragioneSociale);
			st.setDouble(7, testata.getTotaleFattura());
			st.setString(8, testata.getAllegatiJson());
			st.setString(9, testata.getDettagliJson());
			st.setString(10, testata.getRiepiloghiJson());
			st.setString(11, testata.getPagamentiJson());
			st.setString(12, testata.getDataRitiro());
			st.setString(13, testata.getDataSdi1());
			st.setString(14, testata.getDataSdi2());
			st.setString(15, testata.getIdSdi());
			st.setString(16, testata.getStatoSdi());
			st.setString(17, testata.getNomeXml());
			st.setString(18, testata.getNumProEq());
			st.setString(19, testata.getDataProEq());
			st.setString(20, testata.getDataRic());
			st.setString(21, testata.getDataCon());
			st.setString(22, Testo.IMPORTATO);
			st.setString(23, testata.getRegFiscale());
			st.setString(24, causale);
			st.setString(25, testata.getPartitaIvaCliente());
			st.setString(26, testata.getCodiceFiscaleCliente());
			st.setString(27, testata.getBolloVirtuale());
			st.setDouble(28, testata.getImportoBollo());
			execute = st.execute();

			return execute;
		}


		catch(SQLException e){
			logger.error(TAG, e);
			return false;

		}

	}	
	
	public static boolean insertOrdine(Connection connection, Ordine ordine){

		try{

			boolean execute=true;
 
			PreparedStatement st = connection.prepareStatement("INSERT INTO FATTURE (ATT_PASS, TIPO, NUMERO, DATA, PARTITA_IVA, RAGIONE_SOCIALE, NOME_XML, DETTAGLI, STATO) VALUES (?,?,?,?,?,?,?,?,?)");
			st.setString(1,Testo.ORDINE);
			st.setString(2, Testo.ORDINE);
			st.setString(3, ordine.getIdOrder());
			st.setString(4,ordine.getIssueDate());
			st.setString(5, ordine.getPartitaIva().replace("IT", ""));
			st.setString(6,ordine.getName() + "-" + ordine.getCityName().toUpperCase());
			st.setString(7,ordine.getNomeXml());
			st.setString(8,ordine.getIdDelivery());
			st.setString(9, Testo.IMPORTATO);
			
			execute = st.execute();

			return execute;
		}


		catch(SQLException e){
			logger.error(TAG, e);
			return false;

		}

	}	

	public static boolean deleteFatturaByNumero(Connection connection, String tipo, String tipDoc, String datDoc, String numDoc, String piva){

		try{

			boolean execute=true;

			PreparedStatement st = connection.prepareStatement("DELETE FROM FATTURE WHERE ATT_PASS = ? AND TIPO = ? AND DATA = ? AND NUMERO = ? AND PARTITA_IVA = ?");
			st.setString(1, tipo);
			st.setString(2, tipDoc);
			st.setString(3, datDoc);
			st.setString(4, numDoc);
			st.setString(5, piva);
			
			execute = st.execute();
			return execute;
		}


		catch(SQLException e){
			logger.error(TAG, e);
			return false;

		}

	}	

	public static boolean updateFattureByTipoAndStato(Connection connection, String tipo, String stato){

		try{

			boolean execute=true;

			PreparedStatement st = connection.prepareStatement("UPDATE FATTURE SET STATO = ? WHERE STATO = ? AND ATT_PASS = ?");
			st.setString(1,Testo.IMPORTATO);
			st.setString(2,stato);
			st.setString(3,tipo);
			st.executeUpdate();
			return execute;
		}


		catch(SQLException e){
			logger.error(TAG, e);
			return false;

		}

	}
	
	public static boolean updateFattura(Connection connection, Fattura fattura, String statoSdi){
	 	
		String tipo =fattura.getTipo();
		String tipoDoc = fattura.getTipoDocumento();
		String dataDoc = fattura.getData();
		String numDoc = fattura.getNumero();
		String partitaIva = fattura.getPartitaIva();

		try{

			boolean execute=true; 

			PreparedStatement st = connection.prepareStatement("UPDATE FATTURE SET STATO_SDI = ? WHERE NUMERO = ? AND ATT_PASS = ? AND TIPO = ? AND DATA = ? AND PARTITA_IVA = ?");
			st.setString(1,statoSdi);
			st.setString(2,numDoc);
			st.setString(3,tipo);
			st.setString(4,tipoDoc);
			st.setString(5,dataDoc);
			st.setString(6,partitaIva);
			st.executeUpdate();
			return execute;
		}


		catch(SQLException e){
			logger.error(TAG, e);
			return false;

		}

	}

	public static void  creaIndici(Connection connection) throws SQLException{

		boolean execute = false;
		PreparedStatement st=null;

		st = connection.prepareStatement("CREATE INDEX FATTURE_IDX ON FATTURE (ATT_PASS, TIPO, DATA, NUMERO, PARTITA_IVA);");
		execute = st.execute();
		
		st = connection.prepareStatement("CREATE INDEX FATTURE_IDX_2 ON FATTURE (ATT_PASS, STATO);");
		execute = st.execute();
		
		st = connection.prepareStatement("CREATE INDEX FATTURE_IDX_3 ON FATTURE (ID_SDI);");
		execute = st.execute();
		
		st = connection.prepareStatement("CREATE INDEX FATTURE_IDX_4 ON FATTURE (NOME_XML);");
		execute = st.execute();

	}


}
