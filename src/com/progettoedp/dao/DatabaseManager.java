package com.progettoedp.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.progettoedp.main.Application;
import com.progettoedp.testo.Testo;
import com.progettoedp.util.Logger;
 

public class DatabaseManager {

	private static DatabaseManager manager;
	private String type;
	private String databaseName;
	private static final Logger logger = Logger.getLogger(DatabaseManager.class);
	private static final String TAG = DatabaseManager.class.getCanonicalName();
	
	public DatabaseManager(String type, String databaseName){
		this.type = type;
		this.databaseName = databaseName;
	}

//	public static synchronized DatabaseManager getInstance(String type) {
//		if (manager == null) {
//			manager = new DatabaseManager(type);
//		}
//		return manager;
//	}

	public boolean init() {
		// check del driver

		try {
			
			switch (type) {
			case Testo.SQLITE:
				Class.forName("org.sqlite.JDBC");
				break;
			case Testo.FIREBIRD:
				Class.forName("org.firebirdsql.jdbc.FBDriver");
				break;
			case Testo.MSSQL:
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
				break;
			case Testo.AS400:	
				Class.forName("com.ibm.as400.access.AS400JDBCDriver");
				break;
			default:
				break;
			}
				
		} catch (ClassNotFoundException e) {
			// mLogger.e(TAG, "non è stato trovato il driver SQLite");
			return false;
		}

		 
		return true;
	}
 
	public Connection getConnection() {
		
		Connection conn = null;
		try {
			
			switch (type) {
			case Testo.SQLITE:
				conn = DriverManager.getConnection("jdbc:sqlite:database.db");
				break;
			case Testo.MSSQL:
				String host = Application.getInstance().getHostName();
				String port = Application.getInstance().getPortNumber();
				String user = Application.getInstance().getUser();
				String password = Application.getInstance().getPassword();
				String url = null;
				switch (databaseName) {
				case Testo.FATTUREB2B:
					url = "jdbc:sqlserver://" + host +  ";" + "databaseName=" + databaseName;					
					break;
				default:
					url = "jdbc:jtds:sqlserver://" + host + ";" + "DatabaseName=" + databaseName +";encrypt=false";					
					break;
				}
				conn = DriverManager.getConnection(url,user, password);
				break;
			case Testo.AS400:
				String host_as400 = Application.getInstance().getHost_as400();
				String user_as400 = Application.getInstance().getUser_as400();
				String password_as400 = Application.getInstance().getPassword_as400();
				 conn = DriverManager.getConnection("jdbc:as400://" + host_as400, user_as400, password_as400);
				break;
			default:
				break;
			}
			
			
		} catch (SQLException e) {
			logger.error(TAG, "non e' stato possibile accedere al db" +  e);
		}
		return conn;
 
	}

}
